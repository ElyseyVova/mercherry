package ru.autohelp.utils.extensions

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import androidx.core.content.ContextCompat
import android.util.TypedValue
import androidx.fragment.app.Fragment
import com.mlsdev.rximagepicker.RxImagePicker
import com.mlsdev.rximagepicker.Sources
import com.tbruyelle.rxpermissions2.RxPermissions
import org.jetbrains.anko.ctx
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.support.v4.act
import org.jetbrains.anko.support.v4.ctx
import ru.mercherry.R
import ru.mercherry.utils.FileUtils
import ru.mercherry.utils.extensions.instance
import showDialogMessage

fun Context.version() = ctx.packageManager.getPackageInfo(ctx.packageName, 0).versionName ?: "1.0"

fun Context.str(resource: Int): String = resources.getString(resource)

fun Resources.str(resource: Int): String = getString(resource)

fun Context.color(resource: Int) = ContextCompat.getColor(this, resource)

fun Context.colorBackgroundSelectable() = TypedValue().apply {
    theme.resolveAttribute(android.R.attr.selectableItemBackground, this, true)
}.resourceId

fun Context.drawable(resource: Int): Drawable? = ContextCompat.getDrawable(this, resource)


fun Fragment.takeImage(item: Int, fn: (String) -> Unit) {
    when (item) {
        0 -> act.takePhoto { ctx.runOnUiThread { fn.invoke(it) } }
        1 -> act.pickPhoto { ctx.runOnUiThread { fn.invoke(it) } }
    }
}

fun Activity.takePhoto(fn: (String) -> Unit) {
    RxPermissions(this)
        .request(Manifest.permission.CAMERA)
        .subscribe { isGranted ->
            when {
                isGranted -> RxImagePicker.with(this)
                    .requestImage(Sources.CAMERA)
                    .subscribe { fn.invoke(FileUtils.getPath(this, it)) }
                else -> showDialogMessage(getString(R.string.alert_permission_camera))
            }
        }
}

fun Activity.pickPhoto(fn: (String) -> Unit) {
    RxPermissions(this)
        .request(Manifest.permission.READ_EXTERNAL_STORAGE)
        .subscribe { isGranted ->
            when {
                isGranted -> RxImagePicker.with(this)
                    .requestImage(Sources.GALLERY)
                    .subscribe { fn.invoke(FileUtils.getPath(this, it)) }
                else -> showDialogMessage(getString(R.string.alert_permission_storage))
            }
        }
}

fun Drawable.setColor(context: Context, color: Int) {
    this.setColorFilter(context.color(color), PorterDuff.Mode.SRC_IN)
}

fun Drawable.setColorResource(color: Int) {
    this.setColorFilter(color, PorterDuff.Mode.SRC_IN)
}

fun vector(resource: Int) = VectorDrawableCompat.create(instance(), resource, null)
