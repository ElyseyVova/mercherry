package ru.autohelp.utils.extensions

import android.location.Address
import android.location.Location
import android.text.format.DateFormat
import com.google.gson.Gson
import java.util.*
import android.provider.SyncStateContract.Helpers.update
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


val String.formatted
    get() = replace("\\D+".toRegex(), "")

val String.isNotValidPhone: Boolean
    get() = !matches((Regex("^[0-9]{11}+\$")))

val String.isNotValidCode: Boolean
    get() = !matches((Regex("^[0-9]{4,}+\$")))

val String.digits: String get() = replace("\\D+".toRegex(), "")

val String.throwable: Throwable
    get() = Throwable(this)

val String.splitPhone: Array<String>
    get() = arrayOf(substring(2 until 5), substring(5 until length))

val Float.abs: Float
    get() = Math.abs(this)

fun Float?.toString(format: String) = String.format(format, this)

val Long.asTime: String
    get() = DateFormat.format("kk:mm", Date(this * 1000)) as String

val Long.asDay: String
    get() = DateFormat.format("dd MMMM", Date(this * 1000)) as String

val Long.asDate: String
    get() = DateFormat.format("dd MMMM yyyy г.", Date(this * 1000)) as String

val Long.toDate: Date
    get() = Date(this * 1000)

val Long.differentDay: Int
    get() {
        val thatDay = Calendar.getInstance()
        thatDay.time = Date(this * 1000)

        return ((Calendar.getInstance().timeInMillis - thatDay.timeInMillis) / (24 * 60 * 60 * 1000)).toInt()
    }

val <T> Collection<T>.last
    get() = last()

val <T> Collection<T>.isEmpty
    get() = isEmpty()

val <T> Collection<T>.isNotEmpty
    get() = isNotEmpty()

val Any?.json: String
    get() = Gson().toJson(this)

fun <T> MutableList<T>.removeLast() = removeAt(lastIndex)

inline fun <reified T> Any.fromJson(): T? = Gson().fromJson(this as String, T::class.java)

fun pluralForm(number: Int, forms: Array<String>): String {
    return if (number % 10 == 1 && number % 100 != 11) forms[0] else if (number % 10 >= 2 && number % 10 <= 4 && (number % 100 < 10 || number % 100 >= 20)) forms[1] else forms[2]
}


fun Address.toText(): String {
    return "$adminArea, $locality, $thoroughfare, $subThoroughfare".replace("null, ", "")
}

fun Location.asString() = "$latitude,$longitude"

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    val digested = md.digest(toByteArray())
    return digested.joinToString("") {
        String.format("%02x", it)
    }
}

val Long.stringForTime: String
    get() {
        val totalSeconds = this / 1000

        val seconds = totalSeconds % 60
        val minutes = totalSeconds / 60 % 60

        return Formatter(StringBuilder(), Locale.getDefault()).format("%02d:%02d", minutes, seconds).toString()
    }
