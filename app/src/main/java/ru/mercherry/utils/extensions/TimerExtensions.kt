package ru.mercherry.utils.extensions

import android.os.CountDownTimer

fun timer(
    time: Long,
    interval: Long = 1L,
    inverse: Boolean = true,
    onTick: (time: Long) -> Unit,
    onCompleted: () -> Unit,
    autostart: Boolean = false
): CountDownTimer {
    val timer = object : CountDownTimer(time, interval) {
        override fun onFinish() {
            onCompleted.invoke()
        }

        override fun onTick(mills: Long) {
            onTick.invoke(if (inverse) mills else time - mills)
        }
    }
    if (autostart) timer.start()

    return timer
}

fun timer(
    time: Long,
    onCompleted: () -> Unit
): CountDownTimer {
    val timer = object : CountDownTimer(time, time) {
        override fun onFinish() {
            onCompleted.invoke()
        }

        override fun onTick(mills: Long) {
        }
    }

    return timer.start()
}
