package ru.mercherry.utils.extensions

import android.animation.Animator
import android.animation.ObjectAnimator
import android.os.Build
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.core.text.HtmlCompat
import com.alimuzaffar.lib.pin.PinEntryEditText
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.chrisbanes.photoview.PhotoView
import com.redmadrobot.inputmask.MaskedTextChangedListener
import org.jetbrains.anko.inputMethodManager
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.sdk27.coroutines.onCheckedChange
import org.jetbrains.anko.sdk27.coroutines.onSeekBarChangeListener
import org.jetbrains.anko.sdk27.coroutines.onTouch
import ru.autohelp.utils.extensions.setColor
import ru.autohelp.utils.extensions.str
import ru.mercherry.R
import java.util.*
import java.util.concurrent.TimeUnit

fun View.enabled(isEnabled: Boolean) {
    this.isEnabled = isEnabled
}

fun View.enable() {
    this.isEnabled = true
}

fun View.disable() {
    this.isEnabled = false
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.show(isEnabled: Boolean = true) {
    this.visibility = View.VISIBLE
    if (isEnabled) enable() else disable()
}

fun View.changeVisibility() {
    showed(!isShown)
}

fun View.showed(isShowed: Boolean = true) {
    if (isShowed) show() else hide()
}

fun View.visibility(isVisible: Boolean = true) {
    if (isVisible) show() else hide()
}

fun View.delayed(delay: Long = 200, action: () -> Unit) {
    postDelayed(action, delay)
}

fun View.delayed(action: () -> Unit) {
    postDelayed(action, 200)
}

fun View.enableHideKeyboard() {
    onTouch { v, _ -> this@enableHideKeyboard.context.inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0) }
}

fun View.preventDouble() {
    disable()
    postDelayed({ enable() }, 500)
}

fun View.preventDouble(menuItem: MenuItem) {
    menuItem?.isEnabled = false
    postDelayed({ menuItem?.isEnabled = true }, 500)
}

fun ViewGroup.inflate(resource: Int, attachToRoot: Boolean = true): View? =
    context.layoutInflater.inflate(resource, this, attachToRoot)

fun ObjectAnimator.onAnimationEnd(listener: () -> Unit) {
    this.addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(p0: Animator?) {
        }

        override fun onAnimationEnd(p0: Animator?) {
            listener()
        }

        override fun onAnimationCancel(p0: Animator?) {
        }

        override fun onAnimationStart(p0: Animator?) {
        }
    })
}

fun View.slideInTop() {
    if (!isShown) {
//        ViewAnimator
//            .animate(this)
//            .translationY(-1000f, 0f)
//            .duration(300)
//            .start()
        show()
    } else {
//        ViewAnimator
//            .animate(this)
//            .translationY(0f, (-height).toFloat())
//            .duration(300)
//            .onStop { hide() }
//            .start()
        hide()
    }
}


/**
 * Android UI Views
 * **/

/**
 * Button
 * */

fun Button.setTitle(title: String, listener: View.OnClickListener) {
    this.text = title
    this.setOnClickListener(listener)
}


/**
 * TextView
 * */

fun TextView.isEmptyText() = text.toString().isEmpty()

fun TextView.isNotEmptyText() = text.toString().isNotEmpty()

fun TextView.setVisibleText(visibleText: String) {
    if (!isShown) {
        show()
    }
    text = visibleText
}

fun TextView.text() = text.toString()

fun TextView.setTextFromHtml(string: Int) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    HtmlCompat.fromHtml(context.str(string), Html.FROM_HTML_MODE_LEGACY)
} else {
    Html.fromHtml(context.str(string))
}


/**
 * EditText
 * */

fun EditText.clear() = setText("")

fun EditText.addMaskPhone() {
    addTextChangedListener(MaskedTextChangedListener("+{7} ([000]) [000]-[00]-[00]", true, this, null, null))
    setSelection(text.length)
}

fun EditText.addMaskLicensePlate() {
    addTextChangedListener(MaskedTextChangedListener("[A000AA900]", true, this, null, null))
    setSelection(text.length)
}

fun EditText.trimmedText(listener: (String) -> Unit) {
    text.toString().trim().apply {
        listener.takeIf { isNotEmpty() }?.invoke(this)
    }
}

fun EditText.onTextTyping(isEmpty: Boolean = false, fn: () -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {}

        override fun beforeTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(text: CharSequence, p1: Int, p2: Int, p3: Int) {
            fn.takeIf { isEmpty || text.isNotEmpty() }?.invoke()
        }
    })
}

fun EditText.onTextChanged(fn: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {}

        override fun beforeTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(text: CharSequence, p1: Int, p2: Int, p3: Int) {
            fn.invoke(text.toString())
        }
    })
}

fun PinEntryEditText.onTextChanged(fn: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {}

        override fun beforeTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(text: CharSequence, p1: Int, p2: Int, p3: Int) {
            fn.invoke(text.toString())
        }
    })
}

fun EditText.text() = text.toString()

val EditText.formattedPhone: String
    get() = text.toString().replace("\\D+".toRegex(), "")


//====================================================================================================================================================
// Glide - Load image
//====================================================================================================================================================

fun ImageView.loadImage(url: String?) {
    Glide
        .with(this)
        .load(url)
        .apply(RequestOptions().apply {
            //            error(R.drawable.ic_photo)
//            placeholder(R.drawable.ic_photo)
        })
        .into(this)
}

fun ImageView.loadImageAvatar(url: String?) {
    Glide
        .with(this)
        .load(url)
        .apply(RequestOptions().apply {
        })
        .into(this)
}

fun PhotoView.loadImage(url: String?) {
    Glide
        .with(this)
        .load(url)
        .apply(RequestOptions().apply {
            //            error(R.drawable.ic_photo)
//            placeholder(R.drawable.ic_photo)
        })
        .into(this)
}


/**
 * RecyclerView
 * */

fun androidx.recyclerview.widget.RecyclerView.scrollToStart() {
    smoothScrollToPosition(0)
}

fun androidx.recyclerview.widget.RecyclerView.scrollToEnd() {
    scrollToPosition(adapter?.itemCount ?: 1 - 1)
}

fun androidx.recyclerview.widget.RecyclerView.LayoutManager.firstVisiblePosition() =
    (this as androidx.recyclerview.widget.LinearLayoutManager).findFirstVisibleItemPosition()

val androidx.recyclerview.widget.RecyclerView.simpleItemAnimator
    get() = itemAnimator as androidx.recyclerview.widget.SimpleItemAnimator

/**
 * ProgressBar
 * **/

fun ProgressBar.setColor(color: Int) {
    indeterminateDrawable.setColor(context, color)
}

/**
 * Seekbar
 * */

fun SeekBar.onProgressChange(handler: (Int) -> Unit) {
    onSeekBarChangeListener {
        onProgressChanged { seekBar, i, b ->
            handler.invoke(i)
        }
    }

}


/**
 * DatePicker
 * **/

val DatePicker.dateString: String
    get() {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        return calendar.time.toString()
    }

val DatePicker.dateAsSeconds: Long
    get() {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        return TimeUnit.MILLISECONDS.toSeconds(calendar.timeInMillis)
    }


/**
 * Switch
 * */

fun SwitchCompat.setChecked(isCheck: Boolean?, isTriggered: Boolean, onChecked: (Boolean) -> Unit) {
    if (isCheck == null) return

    if (!isTriggered) setOnCheckedChangeListener(null)
    isChecked = isCheck
    onCheckedChange { _, isChecked -> onChecked.invoke(isChecked) }
}


fun Spinner.setAdapter(items: List<String>, listener: (Int) -> Unit) {
//    this.adapter = ArrayAdapter(context, R.layout.item_spinner, items)
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            listener.invoke(position)
        }

    }
}

fun AutoCompleteTextView.setAdapter(items: List<String>, listener: (String) -> Unit) {
//    setAdapter(ArrayAdapter(context, R.layout.item_spinner, items))
    setOnItemClickListener { parent, view, position, id -> listener.invoke((view as TextView).text()) }
}

val RadioGroup.selectedPosition
    get() = indexOfChild(findViewById(checkedRadioButtonId));