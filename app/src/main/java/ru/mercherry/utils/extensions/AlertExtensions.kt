import android.content.Context
import android.graphics.Color
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import androidx.fragment.app.FragmentActivity
import com.elyseev.sheets.model.SheetItem
import com.elyseev.sheets.util.showSheetAction
import com.elyseev.sheets.util.showSheetAlert
import com.elyseev.sheets.util.showSheetDialog
import com.kaopiz.kprogresshud.KProgressHUD
import org.jetbrains.anko.*
import ru.autohelp.utils.extensions.str
import ru.mercherry.utils.extensions.setColor
import ru.mercherry.R

fun Context.showDialogMessage(message: String) {
    showSheetAlert(str(R.string.label_alert_attention), "OK", message)
}

fun Context.showWarningLogout(listenerOk: () -> Unit) {
    showSheetDialog(
        title = str(R.string.label_alert_attention),
        titleOk = str(R.string.button_action_ok),
        titleCancel = str(R.string.button_action_cancel),
        message = str(R.string.setting_alert_info_exit),
        listenerOk = listenerOk
    )
}

fun Context.showSelectorAddPhoto(listener: (Int) -> Unit) {
    showSheetAction(
        title = str(R.string.settings_options_photo_title),
        titleCancel = str(R.string.button_action_cancel),
        items = listOf(
            SheetItem(0, R.drawable.ic_add_photo, str(R.string.settings_options_photo_actions_photo)),
            SheetItem(1, R.drawable.ic_gallery, str(R.string.settings_options_photo_actions_gallery))
        ),
        listenerSelectable = listener
    )
}


fun Context.loader(message: String = str(R.string.state_load)) = KProgressHUD.create(this)
    .setBackgroundColor(Color.WHITE)
    .setCustomView(ProgressBar(this).also { it.setColor(R.color.colorAccent) })
    .setCancellable(false).setDimAmount(0.5f)
    .setDimAmount(0.75f)
    .setLabel("Загрузка...", Color.BLACK)


fun FragmentActivity.hideKeyboard() {
    if (currentFocus != null) {
        inputMethodManager.hideSoftInputFromWindow(currentFocus.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}