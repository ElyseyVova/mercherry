package ru.mercherry.utils.extensions

import android.Manifest
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import org.jetbrains.anko.act
import org.jetbrains.anko.ctx
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.support.v4.act
import org.jetbrains.anko.support.v4.ctx
import ru.autohelp.utils.extensions.pickPhoto
import ru.autohelp.utils.extensions.takePhoto

fun Activity.permissionReceiveSms(listener: (Boolean) -> Unit) {
    RxPermissions(this)
            .request(Manifest.permission.RECEIVE_SMS)
            .subscribe { listener.invoke(it) }
}

fun Activity.permissionStorage(listener: (Boolean) -> Unit) {
    RxPermissions(this)
            .request(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
            .subscribe { listener.invoke(it) }
}

fun Activity.permissionLocation(listener: (Boolean) -> Unit) {
    RxPermissions(this)
            .request(Manifest.permission.ACCESS_FINE_LOCATION)
            .subscribe { listener.invoke(it) }
}

fun FragmentActivity.takeImage(item: Int, fn: (String) -> Unit) {
    when (item) {
        0 -> takePhoto { runOnUiThread { fn.invoke(it) } }
        1 -> pickPhoto { runOnUiThread { fn.invoke(it) } }
    }
}

fun Activity.restartApp() {
//    val intent = Intent(this, SplashActivity::class.java)
//    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//    finish()
//    startActivity(intent)
}
