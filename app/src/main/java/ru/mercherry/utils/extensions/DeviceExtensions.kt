package ru.autohelp.utils.extensions

import android.content.Context
import org.jetbrains.anko.connectivityManager

val Context.isNetworkActive
    get() = connectivityManager.activeNetworkInfo != null

val currentTime: Long
    get() = System.currentTimeMillis()
