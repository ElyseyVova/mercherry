package ru.mercherry.utils.extensions

import android.content.Context
import androidx.fragment.app.Fragment
import ru.mercherry.utils.Constants.SCOPES_APP
import org.jetbrains.anko.support.v4.ctx
import toothpick.Toothpick

inline fun <reified T : Any> instance() = Toothpick.openScope(SCOPES_APP).getInstance(T::class.java)!!

fun Context.inject() = Toothpick.inject(this, Toothpick.openScope(SCOPES_APP))
fun Fragment.inject() = Toothpick.inject(ctx, Toothpick.openScope(SCOPES_APP))
