package ru.autohelp.utils.extensions

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun Disposable.addToComposite(disposable: CompositeDisposable) {
    disposable.add(this)
}

fun <T> Observable<T>.onUiThread(): Observable<T> = this.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
