package ru.mercherry.utils

object Constants {
    const val CAMERA = 0
    const val GALLERY = 1

    const val SET_AVATAR = 0
    const val REMOVE = 1

    const val SCOPES_APP = "Scopes#App"

    const val EDIT_REQUEST = 901

    const val DOMAIN = " https://mercherry.ru/"
}
