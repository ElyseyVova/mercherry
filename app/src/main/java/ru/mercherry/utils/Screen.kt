package ru.mercherry.utils

enum class Screen {
    MAIN,
    AUTHORIZATION,
    REGISTRATION,
    SUPPORT,
    SETTINGS,
    INFO,
    JOB,
    JOB_RESPONSE,
    JOBS,
    MY_JOBS
}
