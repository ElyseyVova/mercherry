package ru.mercherry

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import ru.mercherry.di.DependencyInjection

class App : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        DependencyInjection.configure(this)
    }
}
