package ru.mercherry.data.preferences

import android.content.Context
import android.content.Context.MODE_PRIVATE
import ru.mercherry.data.preferences.Preferences.Constants.PREFERENCES_NAME
import ru.mercherry.data.preferences.Preferences.Constants.PREF_ID
import ru.mercherry.data.preferences.Preferences.Constants.PREF_TOKEN

class Preferences(ctx: Context) {

    object Constants {
        const val PREFERENCES_NAME = "mercherry-preferences"
        const val PREF_ID = "$PREFERENCES_NAME-userId"
        const val PREF_TOKEN = "$PREFERENCES_NAME-token"
    }

    var hasToken: Boolean = false
        get() = token.isNotEmpty()

    var userId: Int
        set(value) = preferences.edit().putInt(PREF_ID, value).apply()
        get() = preferences.getInt(PREF_ID, 0)

    var token: String
        set(value) = preferences.edit().putString(PREF_TOKEN, value).apply()
        get() = preferences.getString(PREF_TOKEN, "")

    private val preferences = ctx.getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE)

    fun clear() {
        preferences.edit().clear().apply()
    }
}
