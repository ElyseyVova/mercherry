package ru.mercherry.data.repository

import retrofit2.http.Field
import ru.mercherry.data.model.Job
import ru.mercherry.data.model.User
import ru.mercherry.data.model.errors.AuthorizationError
import ru.mercherry.data.model.errors.SettingsError
import ru.mercherry.data.model.errors.SupportError
import ru.mercherry.data.model.response.JobsResponse
import ru.mercherry.data.model.result.AuthorizationResult

interface MercherryRepository {

    val isAuthorized: Boolean
    val userId: Int

    fun authorization(
        email: String,
        password: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (AuthorizationError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun registration(
        nickname: String,
        email: String,
        password: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (AuthorizationError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun jobs(
        page: Int,
        city: String?,
        salary: Int?,
        employment: Int?,
        onStart: () -> Unit,
        onSuccess: (JobsResponse) -> Unit,
        onEmpty: () -> Unit = {},
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun myJobs(
        page: Int,
        onStart: () -> Unit,
        onSuccess: (JobsResponse) -> Unit,
        onEmpty: () -> Unit = {},
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun job(
        jobId: Int,
        onStart: () -> Unit,
        onSuccess: (Job) -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun responseJob(
        jobId: Int,
        answers: Map<String, Int>,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun profile(
        onStart: () -> Unit,
        onSuccess: (User) -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun updateProfile(
        username: String,
        firstName: String,
        lastName: String,
        email: String,
        phone: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (SettingsError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun updatePassword(
        newPassword: String,
        confirmPassword: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (SettingsError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun setAvatar(
        pathFile: String,
        onStart: () -> Unit,
        onSuccess: (String) -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun sendMessage(
        name: String,
        email: String,
        body: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (SupportError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun info(
        type: String,
        onStart: () -> Unit,
        onSuccess: (String) -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    )

    fun exit()
}
