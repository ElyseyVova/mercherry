package ru.mercherry.data.repository

import android.annotation.SuppressLint
import com.google.gson.JsonObject
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.HttpException
import retrofit2.Response
import ru.autohelp.utils.extensions.fromJson
import ru.autohelp.utils.extensions.onUiThread
import ru.mercherry.data.api.MercherryAPI
import ru.mercherry.data.model.Error
import ru.mercherry.data.model.Job
import ru.mercherry.data.model.User
import ru.mercherry.data.model.errors.AuthorizationError
import ru.mercherry.data.model.errors.AuthorizationError.*
import ru.mercherry.data.model.errors.SettingsError
import ru.mercherry.data.model.errors.SupportError
import ru.mercherry.data.model.response.AuthorizationResponse
import ru.mercherry.data.model.response.InfoResponse
import ru.mercherry.data.model.response.JobsResponse
import ru.mercherry.data.model.response.SuccessResponse
import ru.mercherry.data.model.result.*
import ru.mercherry.data.preferences.Preferences
import java.io.File
import java.net.UnknownHostException
import javax.inject.Inject

class MercherryRepositoryImpl @Inject constructor(
    private val api: MercherryAPI,
    private val preferences: Preferences
) : MercherryRepository {

    override val isAuthorized: Boolean
        get() = preferences.hasToken

    override val userId: Int
        get() = preferences.userId


    override fun authorization(
        email: String,
        password: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (AuthorizationError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.authorization(email, password)
            .map { responseAuthorizationToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is AuthorizationResult.Success -> onSuccess.invoke()
                    is AuthorizationResult.Failure -> onError.invoke(it.error)
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke(DEFAULT)
                }
            })
    }

    @SuppressLint("CheckResult")
    override fun registration(
        nickname: String,
        email: String,
        password: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (AuthorizationError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.registration(nickname, email, password)
            .map { responseAuthorizationToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is AuthorizationResult.Success -> onSuccess.invoke()
                    is AuthorizationResult.Failure -> onError.invoke(it.error)
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke(DEFAULT)
                }
            })
    }

    override fun myJobs(
        page: Int,
        onStart: () -> Unit,
        onSuccess: (JobsResponse) -> Unit,
        onEmpty: () -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.myJobs(page, "Bearer ${preferences.token}")
            .map { responseJobsToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is JobsResult.Success -> onSuccess.invoke(it.jobsResponse)
                    is JobsResult.Empty -> onEmpty.invoke()
                    is JobsResult.Failure -> onError.invoke()
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke()
                }
            })
    }

    override fun jobs(
        page: Int,
        city: String?,
        salary: Int?,
        employment: Int?,
        onStart: () -> Unit,
        onSuccess: (JobsResponse) -> Unit,
        onEmpty: () -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.jobs(page, city, salary, employment, "Bearer ${preferences.token}")
            .map { responseJobsToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is JobsResult.Success -> onSuccess.invoke(it.jobsResponse)
                    is JobsResult.Empty -> onEmpty.invoke()
                    is JobsResult.Failure -> onError.invoke()
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke()
                }
            })
    }

    override fun job(
        jobId: Int,
        onStart: () -> Unit,
        onSuccess: (Job) -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.job(jobId, "Bearer ${preferences.token}")
            .map { responseJobToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is JobResult.Success -> onSuccess.invoke(it.job)
                    is JobResult.Failure -> onError.invoke()
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke()
                }
            })
    }

    override fun responseJob(
        jobId: Int,
        answers: Map<String, Int>,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.responseJob(jobId, answers, "Bearer ${preferences.token}")
            .map { responseResponseJobToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is SuccessResult.Success -> onSuccess.invoke()
                    is SuccessResult.Failure -> onError.invoke()
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke()
                }
            })
    }

    override fun profile(
        onStart: () -> Unit,
        onSuccess: (User) -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.profile("Bearer ${preferences.token}")
            .map { responseProfileToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is ProfileResult.Success -> onSuccess.invoke(it.user)
                    is ProfileResult.Failure -> onError.invoke()
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke()
                }
            })
    }

    override fun updateProfile(
        username: String,
        firstName: String,
        lastName: String,
        email: String,
        phone: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (SettingsError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.updateProfile(username, firstName, lastName, email, phone, "Bearer ${preferences.token}")
            .map { responseUpdateProfileToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is UpdateProfileResult.Success -> onSuccess.invoke()
                    is UpdateProfileResult.Failure -> onError.invoke(it.error)
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke(SettingsError.DEFAULT)
                }
            })
    }

    override fun updatePassword(
        newPassword: String,
        confirmPassword: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (SettingsError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.updatePassword(newPassword, confirmPassword, "Bearer ${preferences.token}")
            .map { responseUpdateProfileToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is UpdateProfileResult.Success -> onSuccess.invoke()
                    is UpdateProfileResult.Failure -> onError.invoke(it.error)
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke(SettingsError.DEFAULT)
                }
            })
    }

    override fun setAvatar(
        pathFile: String,
        onStart: () -> Unit,
        onSuccess: (String) -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        val photo = File(pathFile)
        val reqFile = RequestBody.create(MediaType.parse("image/*"), photo)
        val body = MultipartBody.Part.createFormData("file", photo.name, reqFile)

        api.setAvatar(body, token = "Bearer ${preferences.token}")
            .map { responseAvatarToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is AvatarResult.Success -> onSuccess.invoke(it.url)
                    is AvatarResult.Failure -> onError.invoke()
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke()
                }
            })
    }

    override fun sendMessage(
        name: String,
        email: String,
        body: String,
        onStart: () -> Unit,
        onSuccess: () -> Unit,
        onError: (SupportError) -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.sendMessage(name, email, body, "Bearer ${preferences.token}")
            .map { responseSupportToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is SupportResult.Success -> onSuccess.invoke()
                    is SupportResult.Failure -> onError.invoke(it.error)
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke(SupportError.DEFAULT)
                }
            })
    }

    override fun info(
        type: String,
        onStart: () -> Unit,
        onSuccess: (String) -> Unit,
        onError: () -> Unit,
        onNetworkError: () -> Unit,
        onFinally: () -> Unit
    ) {
        api.info(type, "Bearer ${preferences.token}")
            .map { responseInfoToResult(it) }
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .doOnSubscribe(this::unsubscribeOnDestroy)
            .subscribe({
                when (it) {
                    is InfoResult.Success -> onSuccess.invoke(it.info)
                    is InfoResult.Failure -> onError.invoke()
                }
            }, {
                when (it) {
                    is HttpException, is UnknownHostException -> onNetworkError.invoke()
                    else -> onError.invoke()
                }
            })
    }

    override fun exit() {
        preferences.clear()
    }

    private val disposables = CompositeDisposable()

    private fun unsubscribeOnDestroy(d: Disposable) {
        disposables.add(d)
    }

    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun responseAuthorizationToResult(response: Response<AuthorizationResponse>): AuthorizationResult = when {
        !response.isSuccessful && response.errorBody() == null -> AuthorizationResult.Failure()
        !response.isSuccessful -> {
            val error = response.errorBody()?.string()?.fromJson<Error>()
            when {
                error?.isNickname != false  -> AuthorizationResult.Failure(NICKNAME.apply { text = error!!.nickname })
                error.isEmail -> AuthorizationResult.Failure(EMAIL.apply { text = error.email })
                error.isPassword -> AuthorizationResult.Failure(PASSWORD.apply { text = error.password })
                else -> AuthorizationResult.Failure()
            }
        }
        else -> {
            preferences.token = response.body()!!.token!!
            AuthorizationResult.Success
        }
    }

    private fun responseJobsToResult(response: Response<JobsResponse>) = when {
        !response.isSuccessful || response.body() == null -> JobsResult.Failure
        response.body()!!.totalPages == 0 -> JobsResult.Empty
        else -> JobsResult.Success(response.body()!!)
    }

    private fun responseJobToResult(response: Response<Job>) = when {
        !response.isSuccessful || response.body() == null -> JobResult.Failure
        else -> JobResult.Success(response.body()!!)
    }

    private fun responseProfileToResult(response: Response<User>) = when {
        !response.isSuccessful || response.body() == null -> ProfileResult.Failure
        else -> ProfileResult.Success(response.body()!!).also {
            preferences.userId = response.body()!!.id
        }
    }


    private fun responseSupportToResult(response: Response<SuccessResponse>) = when {
        !response.isSuccessful && response.errorBody() == null -> SupportResult.Failure()
        !response.isSuccessful -> {
            val error = response.errorBody()?.string()?.fromJson<Error>()
            when {
                error?.isBody != false  -> SupportResult.Failure(SupportError.BODY.apply { text = error!!.nickname })
                error.isEmail -> SupportResult.Failure(SupportError.EMAIL.apply { text = error.email })
                error.isName -> SupportResult.Failure(SupportError.NAME.apply { text = error.email })
                else -> SupportResult.Failure()
            }
        }
        else -> SupportResult.Success
    }

    private fun responseUpdateProfileToResult(response: Response<SuccessResponse>) = when {
        !response.isSuccessful && response.errorBody() == null -> UpdateProfileResult.Failure()
        !response.isSuccessful -> {
            val error = response.errorBody()?.string()?.fromJson<Error>()
            when {
                error?.isNickname != false  -> UpdateProfileResult.Failure(SettingsError.NICKNAME.apply { text = error!!.nickname })
                error.isPassword != false  -> UpdateProfileResult.Failure(SettingsError.PASSWORD.apply { text = error!!.password })
                error.isPasswordRepeat != false  -> UpdateProfileResult.Failure(SettingsError.PASSWORD_REPEAT.apply { text = error!!.passwordRepeat })
                error.isEmail -> UpdateProfileResult.Failure(SettingsError.EMAIL.apply { text = error.email })
                else -> UpdateProfileResult.Failure()
            }
        }
        else -> UpdateProfileResult.Success
    }

    private fun responseAvatarToResult(response: Response<SuccessResponse>) = when {
        !response.isSuccessful || response.body() == null || response.body()!!.status != 1 -> AvatarResult.Failure
        else -> AvatarResult.Success(response.body()?.avatar!!)
    }

    private fun responseSuccessToResult(response: Response<SuccessResponse>) = when {
        !response.isSuccessful || response.body() == null || response.body()!!.status != 1 -> SuccessResult.Failure
        else -> SuccessResult.Success
    }

    private fun responseInfoToResult(response: Response<InfoResponse>) = when {
        !response.isSuccessful || response.body() == null || response.body()!!.text.isEmpty() -> InfoResult.Failure
        else -> InfoResult.Success(response.body()!!.text)
    }

    private fun responseResponseJobToResult(response: Response<JsonObject>) = when {
        !response.isSuccessful || response.body() == null -> SuccessResult.Failure
        else -> SuccessResult.Success
    }
}
