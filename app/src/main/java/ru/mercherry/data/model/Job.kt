package ru.mercherry.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class Job : Serializable {

    @SerializedName("id")
    val id = 0

    @SerializedName("label")
    val label = ""

    @SerializedName("type")
    val type = ""

    @SerializedName("city")
    val city = ""

    @SerializedName("metro")
    val metro = ""

    @SerializedName("total_salary")
    val totalSalary = 0

    @SerializedName("employment")
    val employment = ""

    @SerializedName("project")
    val project = ""

    @SerializedName("product_type")
    val productType = ""

    @SerializedName("district")
    val district = ""

    @SerializedName("description")
    val description = ""

    @SerializedName("schedule")
    val schedule = ""

    @SerializedName("point_count_day")
    val pointCount = ""

    @SerializedName("experience")
    val experience = ""

    @SerializedName("duration_internship")
    val durationInternship = ""

    @SerializedName("salary")
    val salary = 0

    @SerializedName("prize")
    val prize = 0

    @SerializedName("weekend")
    val weekend = ""

    @SerializedName("m_book")
    val mediceBook = false

    @SerializedName("mobile")
    val mobile = false

    @SerializedName("auto")
    val auto = false

    @SerializedName("audio_record")
    val audioRecord = false

    @SerializedName("fields")
    val fields = listOf<String>()

    @SerializedName("response")
    val response: Response? = null

    @SerializedName("questions")
    val questions: Map<String, String>? = null

    val fullLocation: String
        get() = city + if (metro.isNotEmpty()) ", м.$metro" else ""

    var hasQuestions: Boolean = false
        get() = questions != null && questions.isNotEmpty()

    data class Response(val status: Int, val text: String, val list: List<String>)
}
