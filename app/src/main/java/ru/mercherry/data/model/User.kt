package ru.mercherry.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class User : Serializable {

    @SerializedName("id")
    var id: Int = 0

    @SerializedName("username")
    var username: String = ""

    @SerializedName("avatar")
    var avatar: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("networks")
    var networks: List<Network>? = null

    @SerializedName("invite_url")
    var inviteUrl: String? = null
}