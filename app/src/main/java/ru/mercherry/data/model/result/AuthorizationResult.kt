package ru.mercherry.data.model.result

import ru.mercherry.data.model.errors.AuthorizationError

sealed class AuthorizationResult {

    data class Failure(val error: AuthorizationError = AuthorizationError.DEFAULT) : AuthorizationResult()
    object Success : AuthorizationResult()
}
