package ru.mercherry.data.model.result

import ru.mercherry.data.model.User

sealed class AvatarResult {
    data class Success(val url: String) : AvatarResult()
    object Failure : AvatarResult()
}
