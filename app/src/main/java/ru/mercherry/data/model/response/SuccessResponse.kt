package ru.mercherry.data.model.response

import com.google.gson.annotations.SerializedName

class SuccessResponse {

    @SerializedName("status")
    val status: Int = 1

    @SerializedName("avatar")
    val avatar: String = ""
}
