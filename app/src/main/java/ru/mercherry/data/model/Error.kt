package ru.mercherry.data.model

open class Error {

    val error: Data? = null

    val isPassword
        get() = error?.password != null && error.password.isNotEmpty()

    val isPasswordRepeat
        get() = error?.password_repeat != null && error.password_repeat.isNotEmpty()

    val isNickname
        get() = error?.username != null && error.username.isNotEmpty()

    val isEmail
        get() = error?.email != null && error.email.isNotEmpty()

    val isBody
        get() = error?.body != null && error.body.isNotEmpty()

    val isName
        get() = error?.name != null && error.name.isNotEmpty()

    val email
        get() = error?.email ?: ""

    val password
        get() = error?.password ?: ""

    val passwordRepeat
        get() = error?.password_repeat ?: ""

    val nickname
        get() = error?.username ?: ""

    val name
        get() = error?.name ?: ""

    val body
        get() = error?.body ?: ""

    class Data {

        val password: String? = ""
        val username: String? = ""
        val email: String? = ""
        val body: String? = ""
        val name: String? = ""
        val password_repeat: String? = ""
    }
}
