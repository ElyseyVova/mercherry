package ru.mercherry.data.model

import java.io.Serializable

class Network : Serializable {
    var id: Int = 0
    var name: String = ""
    var address: Map<String, String>? = null
}