package ru.mercherry.data.model.result

import ru.mercherry.data.model.User

sealed class ProfileResult {
    data class Success(val user: User) : ProfileResult()
    object Failure : ProfileResult()
}
