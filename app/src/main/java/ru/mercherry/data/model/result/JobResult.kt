package ru.mercherry.data.model.result

import ru.mercherry.data.model.Job

sealed class JobResult {
    data class Success(val job: Job) : JobResult()
    object Failure : JobResult()
}
