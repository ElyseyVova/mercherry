package ru.mercherry.data.model.response

import com.google.gson.annotations.SerializedName
import ru.mercherry.data.model.Job
import java.lang.Error

class JobsResponse: Error() {
    @SerializedName("result") val result: List<Job> = listOf()
    @SerializedName("totalPages") val totalPages: Int = 0
    @SerializedName("currentPage") val currentPage: Int = 1
}
