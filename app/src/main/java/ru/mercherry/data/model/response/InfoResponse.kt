package ru.mercherry.data.model.response

import com.google.gson.annotations.SerializedName

class InfoResponse {

    @SerializedName("text")
    val text: String = ""
}
