package ru.mercherry.data.model.errors

enum class AuthorizationError(var text: String) {
    DEFAULT("Что-то пошло не так"),
    EMAIL_EMPTY("Поле не может быть пустым"),
    NICKNAME_EMPTY("Поле не может быть пустым"),
    PASSWORD_EMPTY(("Поле не может быть пустым")),
    UNCHECKED_POLICY(("Необходимо подтвердить согласие на обраьотку персональныйх данных")),
    EMAIL(""),
    PASSWORD(""),
    NICKNAME("")
}

enum class SupportError(var text: String) {
    DEFAULT("Что-то пошло не так"),
    NAME_EMPTY("Поле не может быть пустым"),
    EMAIL_EMPTY("Поле не может быть пустым"),
    BODY_EMPTY(("Поле не может быть пустым")),
    NAME(""),
    EMAIL(""),
    BODY("")
}

enum class SettingsError(var text: String) {
    DEFAULT("Что-то пошло не так"),
    FILE_SMALL("Изображение не подходит"),
    NICKNAME(""),
    EMAIL(""),
    PASSWORD(""),
    PASSWORD_REPEAT("")
}