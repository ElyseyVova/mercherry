package ru.mercherry.data.model.response

import com.google.gson.annotations.SerializedName
import java.lang.Error

class AuthorizationResponse: Error() {
    @SerializedName("token") val token: String? = ""
    @SerializedName("expired ") val expired: Long? = 0
}
