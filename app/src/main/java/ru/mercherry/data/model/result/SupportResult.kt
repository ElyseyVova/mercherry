package ru.mercherry.data.model.result

import ru.mercherry.data.model.User
import ru.mercherry.data.model.errors.AuthorizationError
import ru.mercherry.data.model.errors.SupportError

sealed class SupportResult {
    object Success : SupportResult()
    data class Failure(val error: SupportError = SupportError.DEFAULT) : SupportResult()

}
