package ru.mercherry.data.model.result

import ru.mercherry.data.model.User

sealed class SuccessResult {
    object Success : SuccessResult()
    object Failure : SuccessResult()
}
