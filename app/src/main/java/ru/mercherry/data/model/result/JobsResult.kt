package ru.mercherry.data.model.result

import ru.mercherry.data.model.response.JobsResponse

sealed class JobsResult {
    data class Success(val jobsResponse: JobsResponse) : JobsResult()
    object Empty : JobsResult()
    object Failure : JobsResult()
}
