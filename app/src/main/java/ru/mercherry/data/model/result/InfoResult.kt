package ru.mercherry.data.model.result

import ru.mercherry.data.model.User

sealed class InfoResult {
    data class Success(val info: String) : InfoResult()
    object Failure : InfoResult()
}
