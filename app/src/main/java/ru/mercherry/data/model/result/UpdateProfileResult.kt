package ru.mercherry.data.model.result

import ru.mercherry.data.model.User
import ru.mercherry.data.model.errors.AuthorizationError
import ru.mercherry.data.model.errors.SettingsError
import ru.mercherry.data.model.errors.SupportError

sealed class UpdateProfileResult {
    object Success : UpdateProfileResult()
    data class Failure(val error: SettingsError = SettingsError.DEFAULT) : UpdateProfileResult()

}
