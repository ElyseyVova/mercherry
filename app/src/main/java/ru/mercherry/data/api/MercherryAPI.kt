package ru.mercherry.data.api

import com.google.gson.JsonObject
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*
import ru.mercherry.data.model.Job
import ru.mercherry.data.model.User
import ru.mercherry.data.model.response.AuthorizationResponse
import ru.mercherry.data.model.response.InfoResponse
import ru.mercherry.data.model.response.JobsResponse
import ru.mercherry.data.model.response.SuccessResponse

interface MercherryAPI {

    // ===============================================================================================================================================
    // AUTH
    // ===============================================================================================================================================

    @FormUrlEncoded
    @POST("v1/auth/login")
    fun authorization(
        @Field("login") login: String,
        @Field("password") password: String
    ): Observable<Response<AuthorizationResponse>>

    @FormUrlEncoded
    @POST("v1/auth/signup")
    fun registration(
        @Field("username") nickname: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Observable<Response<AuthorizationResponse>>


    // ===============================================================================================================================================
    // JOB
    // ===============================================================================================================================================

    @GET("v1/jobs")
    fun jobs(
        @Query("page") page: Int,
        @Query("city") city: String?,
        @Query("total_salary") salary: Int?,
        @Query("employment") employment: Int?,
        @Header("Authorization") token: String
    ): Observable<Response<JobsResponse>>

    @GET("v1/jobs/{jobsId}")
    fun job(
        @Path("jobsId") jobsId: Int,
        @Header("Authorization") token: String
    ): Observable<Response<Job>>

    @GET("v1/jobs/profile")
    fun myJobs(
        @Query("page") page: Int,
        @Header("Authorization") token: String
    ): Observable<Response<JobsResponse>>

    @FormUrlEncoded
    @POST("v1/jobs/response/{jobsId}")
    fun responseJob(
        @Path("jobsId") jobsId: Int,
        @FieldMap parameters: Map<String, Int>,
        @Header("Authorization") token: String
    ): Observable<Response<JsonObject>>


    // ===============================================================================================================================================
    // PROFILE
    // ===============================================================================================================================================

    @GET("v1/profile")
    fun profile(
        @Header("Authorization") token: String
    ): Observable<Response<User>>

    @FormUrlEncoded
    @POST("v1/profile/update")
    fun updateProfile(
        @Field("username") username: String,
        @Field("first_name") firstName: String,
        @Field("last_name") lastName: String,
        @Field("email") email: String,
        @Field("phone") phone: String,
        @Header("Authorization") token: String
    ): Observable<Response<SuccessResponse>>

    @FormUrlEncoded
    @POST("v1/profile/password")
    fun updatePassword(
        @Field("password") password: String,
        @Field("password_repeat") password_repeat: String,
        @Header("Authorization") token: String
    ): Observable<Response<SuccessResponse>>

    @Multipart
    @POST("v1/profile/avatar")
    fun setAvatar(
        @Part file: MultipartBody.Part,
        @Header("Authorization") token: String
    ): Observable<Response<SuccessResponse>>


    // ===============================================================================================================================================
    // INFO
    // ===============================================================================================================================================

    @FormUrlEncoded
    @POST("v1/info/contact")
    fun sendMessage(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("body") body: String,
        @Header("Authorization") token: String
    ): Observable<Response<SuccessResponse>>

    @GET("v1/info/{type}")
    fun info(
        @Path("type") type: String,
        @Header("Authorization") token: String
    ): Observable<Response<InfoResponse>>
}
