package ru.mercherry.ui.common.status

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_status_view.view.*
import org.jetbrains.anko.backgroundResource
import ru.mercherry.R
import ru.mercherry.data.model.Job
import ru.mercherry.utils.extensions.inflate
import ru.mercherry.utils.extensions.show

class StatusView @JvmOverloads constructor(ctx: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    LinearLayout(ctx, attrs, defStyleAttr) {

    init {
        inflate(R.layout.view_status_view)
    }

    fun setResponse(response: Job.Response) {
        show()

        when (response.status) {
            50 -> tvStatus.text =
                "Благодарим что откликнулись на эту вакансию, судя по вашим ответам, она вас до конца не устроит. Просим выбрать другую вакансию или задание. Нам важно, чтобы вакансии соответствовали вашим потребностям.\n"
            else -> tvStatus.text =
                "Вы откликнулись на вакансию, работодатель скоро рассмотрит ее. Статус по вашему отклику смотрите в разделе вакансии в личном кабинете"
        }

        if (response.status == 50) {
            tvStatus.backgroundResource = R.drawable.border_red
        } else {
            response.list
                .map { ItemStatusView(context).apply { title = it } }
                .apply {
                    firstOrNull()?.setFirst()
                    lastOrNull()?.setLast()
                    get(response.status).status = ItemStatusView.Status.CURRENT
                }
                .forEachIndexed { index, view ->
                    if (index == response.status) view.status = ItemStatusView.Status.CURRENT
                    if (index < response.status) view.status = ItemStatusView.Status.DONE
                    root.addView(view)
                }
        }
    }
}
