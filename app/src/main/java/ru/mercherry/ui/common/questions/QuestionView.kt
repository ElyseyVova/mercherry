package ru.mercherry.ui.common.questions

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.elyseev.sheets.util.inflate
import kotlinx.android.synthetic.main.view_question.view.*
import ru.mercherry.R
import ru.mercherry.utils.extensions.selectedPosition

class QuestionView @JvmOverloads constructor(ctx: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, var key: String, var question: String) :
    LinearLayout(ctx, attrs, defStyleAttr) {

    val isChecked: Boolean
        get() = groupAnswers.selectedPosition != -1

    val isYes: Boolean
        get() = groupAnswers.selectedPosition == 0

    init {
        inflate(R.layout.view_question)
        tvTitle.text = question
    }
}
