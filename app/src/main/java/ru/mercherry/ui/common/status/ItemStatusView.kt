package ru.mercherry.ui.common.status

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.elyseev.sheets.util.hide
import com.elyseev.sheets.util.inflate
import kotlinx.android.synthetic.main.view_status_view_item.view.*
import ru.autohelp.utils.extensions.setColor
import ru.mercherry.R
import ru.mercherry.ui.common.status.ItemStatusView.Status.*

class ItemStatusView @JvmOverloads constructor(ctx: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    LinearLayout(ctx, attrs, defStyleAttr) {

    var title: String = ""
        set(value) {
            field = value
            tvTitle.text = value
        }

    var status: Status = DEFAULT
        set(value) {
            field = value
            when(value) {
                DEFAULT -> ivStatus.drawable.setColor(context, R.color.divider)
                CURRENT -> ivStatus.drawable.setColor(context, R.color.action_red)
                DONE -> ivStatus.drawable.setColor(context, R.color.action_green)
            }
        }

    init {
        inflate(R.layout.view_status_view_item)
    }

    enum class Status {
        DEFAULT, CURRENT, DONE
    }

    fun setFirst() {
        dividerTop.hide()
    }

    fun setLast() {
        dividerBottom.hide()
    }
}
