package ru.mercherry.ui.screens.my_jobs

import ru.mercherry.base.BaseView
import ru.mercherry.data.model.Job
import ru.mercherry.data.model.errors.AuthorizationError

interface MyJobsView : BaseView {
    fun setJobs(jobs: List<Job>)
    fun addJobs(jobs: List<Job>)
}
