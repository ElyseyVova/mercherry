package ru.mercherry.ui.screens.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick
import ru.mercherry.R
import ru.mercherry.base.BaseActivity
import ru.mercherry.data.model.errors.AuthorizationError
import ru.mercherry.data.model.errors.AuthorizationError.*
import ru.mercherry.utils.extensions.enableHideKeyboard
import ru.mercherry.utils.extensions.instance
import ru.mercherry.utils.extensions.text

class LoginActivity : BaseActivity(), LoginView {

    companion object {
        fun intent(ctx: Context) = ctx.intentFor<LoginActivity>()
    }


    @InjectPresenter
    lateinit var presenter: LoginPresenter


    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupToolbar()
        setupUI()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.handleResult(requestCode, resultCode)
    }

    override fun setupUI() {
        super.setupUI()
        btnEnter.onClick {
            inputEmail.isErrorEnabled = false
            inputPassword.isErrorEnabled = false
            presenter.handleEnterClick(etEmail.text(), etPassword.text())
        }
        btnRegistration.onClick { presenter.handleRegistrationClick() }
        root.enableHideKeyboard()
    }


    // ===============================================================================================================================================
    // View
    // ===============================================================================================================================================

    override fun showError(error: AuthorizationError) {
        when (error) {
            EMAIL -> showError(inputEmail, error.text)
            PASSWORD -> showError(inputPassword, error.text)
            EMAIL_EMPTY -> showError(inputEmail, error.text)
            PASSWORD_EMPTY -> showError(inputPassword, error.text)
        }
    }


    // ===============================================================================================================================================
    // PRIVATE
    // ===============================================================================================================================================

    private fun showError(input: TextInputLayout, text: String) {
        input.isErrorEnabled = true
        input.error = text
    }

    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = instance<LoginPresenter>()
}
