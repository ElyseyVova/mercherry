package ru.mercherry.ui.screens.support

import com.arellomobile.mvp.InjectViewState
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.model.errors.SupportError
import ru.mercherry.data.model.errors.SupportError.*
import ru.mercherry.data.repository.MercherryRepository
import javax.inject.Inject

@InjectViewState
class SupportPresenter @Inject constructor(
    private val repository: MercherryRepository
) : BasePresenter<SupportView>() {

    fun handleSendClick(name: String, email: String, body: String) {
        when {
            name.isEmpty() -> proceedError(NAME_EMPTY)
            email.isEmpty() -> proceedError(EMAIL_EMPTY)
            body.isEmpty() -> proceedError(BODY_EMPTY)
            else -> sendMessage(name, email, body)
        }
    }


    // ===============================================================================================================================================
    // API
    // ===============================================================================================================================================

    private fun sendMessage(name: String, email: String, body: String) {
        repository.sendMessage(
            name = name,
            email = email,
            body = body,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessSendMessage,
            onError = ::proceedError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun proceedSuccessSendMessage() {
        viewState.closeScreen("Ваш вопрос был отправлен")
    }

    private fun proceedError(error: SupportError) {
        when (error) {
            DEFAULT -> viewState.showMessage(error.text)
            else -> viewState.showError(error)
        }
    }
}
