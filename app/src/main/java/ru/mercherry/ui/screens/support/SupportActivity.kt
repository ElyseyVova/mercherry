package ru.mercherry.ui.screens.support

import android.content.Context
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_support.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick
import ru.mercherry.R
import ru.mercherry.base.BaseActivity
import ru.mercherry.data.model.errors.SupportError
import ru.mercherry.data.model.errors.SupportError.*
import ru.mercherry.utils.extensions.instance
import ru.mercherry.utils.extensions.text

class SupportActivity : BaseActivity(), SupportView {

    companion object {
        fun intent(ctx: Context) = ctx.intentFor<SupportActivity>()
    }


    @InjectPresenter
    lateinit var presenter: SupportPresenter

    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)
        setupToolbar()
        setupUI()
    }

    override fun setupUI() {
        super.setupUI()
        btnSend.onClick {
            inputName.isErrorEnabled = false
            inputEmail.isErrorEnabled = false
            inputBody.isErrorEnabled = false
            presenter.handleSendClick(etName.text(), etEmail.text(), etBody.text()) }
    }

    // ===============================================================================================================================================
    // View
    // ===============================================================================================================================================

    override fun showError(error: SupportError) {
        when (error) {
            NAME, NAME_EMPTY -> showFieldError(inputName, error.text)
            EMAIL, EMAIL_EMPTY -> showFieldError(inputEmail, error.text)
            BODY, BODY_EMPTY -> showFieldError(inputBody, error.text)
        }
    }


    // ===============================================================================================================================================
    // PRIVATE
    // ===============================================================================================================================================

    private fun showFieldError(input: TextInputLayout, text: String) {
        input.isErrorEnabled = true
        input.error = text
    }

    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = instance<SupportPresenter>()
}
