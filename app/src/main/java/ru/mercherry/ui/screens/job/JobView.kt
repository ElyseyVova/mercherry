package ru.mercherry.ui.screens.job

import ru.mercherry.base.BaseView
import ru.mercherry.data.model.Job
import ru.mercherry.data.model.errors.AuthorizationError

interface JobView : BaseView {
    fun setJobInfo(job: Job)
}
