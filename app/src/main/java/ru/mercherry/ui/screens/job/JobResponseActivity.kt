package ru.mercherry.ui.screens.job

import android.content.Context
import android.os.Bundle
import androidx.core.view.children
import kotlinx.android.synthetic.main.activity_job_response.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick
import ru.mercherry.R
import ru.mercherry.base.BaseActivity
import ru.mercherry.ui.common.questions.QuestionView

class JobResponseActivity : BaseActivity() {

    companion object {
        fun intent(ctx: Context, questions: Map<String, String>) =
            ctx.intentFor<JobResponseActivity>("questions" to questions)
    }

    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_response)
        setupToolbar()
        setupUI()
    }

    override fun setupUI() {
        super.setupUI()
        val questions = intent.extras.getSerializable("questions") as Map<String, String>

        questions.forEach {
            root.addView(QuestionView(this, key = it.key, question = it.value))
        }

        btnResponseJob.onClick {
            val answers = hashMapOf<String, Int>()
            var isAllChecked = true

            root.children.forEachIndexed { index, view ->
                if (view is QuestionView) {
                    isAllChecked = view.isChecked
                    answers.put(view.key, if (view.isYes) 1 else 0)
                }
            }

            when {
                !isAllChecked -> showAlertMessage("Ответьте на все вопросы в анкете")
                else -> closeScreenWithResult(RESULT_OK, answers)
            }
        }
    }
}
