package ru.mercherry.ui.screens.registration

import ru.mercherry.base.BaseView
import ru.mercherry.data.model.errors.AuthorizationError

interface RegistrationView : BaseView {
    fun showError(error: AuthorizationError)
}
