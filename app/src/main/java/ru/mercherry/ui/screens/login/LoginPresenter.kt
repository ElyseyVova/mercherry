package ru.mercherry.ui.screens.login

import android.app.Activity
import android.app.Activity.RESULT_OK
import com.arellomobile.mvp.InjectViewState
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.model.errors.AuthorizationError
import ru.mercherry.data.model.errors.AuthorizationError.*
import ru.mercherry.data.repository.MercherryRepository
import ru.mercherry.utils.Screen
import javax.inject.Inject

@InjectViewState
class LoginPresenter @Inject constructor(
    private val repository: MercherryRepository
) : BasePresenter<LoginView>() {

    fun handleResult(requestCode: Int, resultCode: Int) {
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) proceedSuccessAuthorization()
    }

    fun handleEnterClick(email: String, password: String) {
        when {
            email.isEmpty() -> proceedError(EMAIL_EMPTY)
            password.isEmpty() -> proceedError(PASSWORD_EMPTY)
            else -> authorization(email, password)
        }
    }

    fun handleRegistrationClick() {
        viewState.addScreenForResult(Screen.REGISTRATION, 100)
    }


    // ===============================================================================================================================================
    // API
    // ===============================================================================================================================================

    private fun authorization(login: String, password: String) {
        repository.authorization(
            email = login,
            password = password,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessAuthorization,
            onError = ::proceedError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun proceedSuccessAuthorization() {
        viewState.showMessage("Вы успешно авторизовались")
        viewState.closeScreenWithResult(RESULT_OK)
    }

    private fun proceedError(error: AuthorizationError) {
        when (error) {
            DEFAULT -> viewState.showMessage(error.text)
            else -> viewState.showError(error)
        }
    }
}
