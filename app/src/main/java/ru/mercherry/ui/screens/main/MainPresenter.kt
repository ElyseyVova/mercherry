package ru.mercherry.ui.screens.main

import android.app.Activity
import android.view.MenuItem
import com.arellomobile.mvp.InjectViewState
import ru.mercherry.R
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.model.User
import ru.mercherry.data.repository.MercherryRepository
import ru.mercherry.utils.Screen
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(
    private val repository: MercherryRepository
) : BasePresenter<MainView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        fetchProfile()
    }

    fun handleResult(requestCode: Int, resultCode: Int) {
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) viewState.showAuthorizedItems(true)
    }

    fun handleItemSelected(item: MenuItem) {
        when(item.itemId) {
            R.id.navigation_jobs -> viewState.addScreen(Screen.JOBS)
            R.id.navigation_support -> viewState.addScreen(Screen.SUPPORT)
            R.id.navigation_my_jobs -> viewState.addScreen(Screen.MY_JOBS)
            R.id.navigation_settings -> viewState.addScreen(Screen.SETTINGS)
            R.id.navigation_authorization -> viewState.addScreenForResult(Screen.AUTHORIZATION, 100)
            R.id.navigation_offer -> viewState.addScreen(Screen.INFO, "offer")
            R.id.navigation_personal -> viewState.addScreen(Screen.INFO, "personal")
            R.id.navigation_exit -> handleExitClick()
        }
    }

    // ===============================================================================================================================================
    // API
    // ===============================================================================================================================================

    private fun fetchProfile() {
        repository.profile(
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessProfile,
            onError = viewState::showError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun handleExitClick() {
        repository.exit()
        viewState.showScreen(Screen.MAIN)
    }

    private fun proceedSuccessProfile(user: User) {
        viewState.showAuthorizedItems(true)
    }
}
