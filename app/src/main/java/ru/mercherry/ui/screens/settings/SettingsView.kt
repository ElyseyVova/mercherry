package ru.mercherry.ui.screens.settings

import ru.mercherry.base.BaseView
import ru.mercherry.data.model.User
import ru.mercherry.data.model.errors.SettingsError
import ru.mercherry.data.model.errors.SupportError

interface SettingsView : BaseView {
    fun setUserInfo(user: User)
    fun setAvatar(url: String)
    fun showPhotoPicker()
    fun showError(error: SettingsError)
}
