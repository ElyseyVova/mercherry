package ru.mercherry.ui.screens.registration

import android.content.Context
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_registration.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick
import ru.mercherry.R
import ru.mercherry.base.BaseActivity
import ru.mercherry.data.model.errors.AuthorizationError
import ru.mercherry.data.model.errors.AuthorizationError.*
import ru.mercherry.utils.extensions.enableHideKeyboard
import ru.mercherry.utils.extensions.instance
import ru.mercherry.utils.extensions.text

class RegistrationActivity : BaseActivity(), RegistrationView {

    companion object {
        fun intent(ctx: Context) = ctx.intentFor<RegistrationActivity>()
    }

    @InjectPresenter
    lateinit var presenter: RegistrationPresenter


    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        setupToolbar()
        setupUI()
    }

    override fun setupUI() {
        super.setupUI()
        btnRegistration.onClick {
            inputNickname.isErrorEnabled = false
            inputEmail.isErrorEnabled = false
            inputPassword.isErrorEnabled = false
            presenter.handleRegisterClick(
                etNickname.text(),
                etEmail.text(),
                etPassword.text(),
                cbPrivacyPolicy.isChecked
            )
        }
         btnAuthorization.onClick { presenter.handleAuthorizationClick() }
        root.enableHideKeyboard()
    }


    // ===============================================================================================================================================
    // View
    // ===============================================================================================================================================

    override fun showError(error: AuthorizationError) {
        when (error) {
            NICKNAME, NICKNAME_EMPTY -> showFieldError(inputNickname, error.text)
            EMAIL, EMAIL_EMPTY -> showFieldError(inputEmail, error.text)
            PASSWORD, PASSWORD_EMPTY -> showFieldError(inputPassword, error.text)
        }
    }


    // ===============================================================================================================================================
    // PRIVATE
    // ===============================================================================================================================================

    private fun showFieldError(input: TextInputLayout, text: String) {
        input.isErrorEnabled = true
        input.error = text
    }

    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = instance<RegistrationPresenter>()

}
