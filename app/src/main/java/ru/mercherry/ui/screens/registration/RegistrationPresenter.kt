package ru.mercherry.ui.screens.registration

import android.app.Activity.RESULT_OK
import com.arellomobile.mvp.InjectViewState
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.model.errors.AuthorizationError
import ru.mercherry.data.model.errors.AuthorizationError.*
import ru.mercherry.data.repository.MercherryRepository
import ru.mercherry.utils.Screen
import javax.inject.Inject

@InjectViewState
class RegistrationPresenter @Inject constructor(
    private val repository: MercherryRepository
) : BasePresenter<RegistrationView>() {

    fun handleRegisterClick(nickname: String, email: String, password: String, isCheckedPolicy: Boolean) {
        when {
            nickname.isEmpty() -> proceedError(NICKNAME_EMPTY)
            email.isEmpty() -> proceedError(EMAIL_EMPTY)
            password.isEmpty() -> proceedError(PASSWORD_EMPTY)
            !isCheckedPolicy -> proceedError(UNCHECKED_POLICY)
            else -> registration(nickname, email, password)
        }
    }

    fun handleAuthorizationClick() {
        viewState.closeScreen()
    }


    // ===============================================================================================================================================
    // API
    // ===============================================================================================================================================

    private fun registration(nickname: String, email: String, password: String) {
        repository.registration(
            nickname = nickname,
            email = email,
            password = password,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessRegistration,
            onError = ::proceedError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun proceedSuccessRegistration() {
        viewState.showMessage("Вы успешно авторизовались")
        viewState.closeScreenWithResult(RESULT_OK)
    }

    private fun proceedError(error: AuthorizationError) {
        when (error) {
            DEFAULT -> viewState.showMessage(error.text)
            UNCHECKED_POLICY -> viewState.showMessage(error.text)
            else -> viewState.showError(error)
        }
    }
}
