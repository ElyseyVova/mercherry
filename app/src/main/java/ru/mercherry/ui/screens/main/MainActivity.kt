package ru.mercherry.ui.screens.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.intentFor
import ru.mercherry.R
import ru.mercherry.base.BaseActivity
import ru.mercherry.utils.extensions.instance

class MainActivity : BaseActivity(), MainView {

    companion object {
        fun intent(ctx: Context) = ctx.intentFor<MainActivity>()
    }


    @InjectPresenter
    lateinit var presenter: MainPresenter


    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupToolbar()
        setupUI()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.handleResult(requestCode, resultCode)
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun setupToolbar() {
        super.setupToolbar()
        setSupportActionBar(toolbar)

        ActionBarDrawerToggle(
            this,
            drawer,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        ).let {
            drawer.addDrawerListener(it)
            it.syncState()
        }
        navigationView.setNavigationItemSelectedListener {
            presenter.handleItemSelected(it)
            drawer.closeDrawer(GravityCompat.START)
            true
        }
    }

    override fun setupUI() {
        super.setupUI()
        presenter.handleItemSelected(navigationView.menu.findItem(R.id.navigation_jobs))
    }

    override fun setTitle(title: CharSequence?) {
        toolbar.title = title
    }

    // ===============================================================================================================================================
    // View
    // ===============================================================================================================================================

    override fun showAuthorizedItems(isAuthorized: Boolean) {
        navigationView.menu.findItem(R.id.navigation_my_jobs).isVisible = isAuthorized
        navigationView.menu.findItem(R.id.navigation_settings).isVisible = isAuthorized
        navigationView.menu.findItem(R.id.navigation_authorization).isVisible = !isAuthorized
        navigationView.menu.findItem(R.id.navigation_exit).isVisible = isAuthorized
    }


    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = instance<MainPresenter>()
}
