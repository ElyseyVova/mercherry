package ru.mercherry.ui.screens.splash

import android.content.Context
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import org.jetbrains.anko.intentFor
import ru.mercherry.R
import ru.mercherry.base.BaseActivity
import ru.mercherry.utils.extensions.instance

class SplashActivity : BaseActivity(), SplashView {

    companion object {
        fun intent(ctx: Context) = ctx.intentFor<SplashActivity>()
    }


    @InjectPresenter
    lateinit var presenter: SplashPresenter

    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }


    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = instance<SplashPresenter>()
}
