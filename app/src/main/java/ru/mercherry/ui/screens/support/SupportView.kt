package ru.mercherry.ui.screens.support

import ru.mercherry.base.BaseView
import ru.mercherry.data.model.errors.SupportError

interface SupportView : BaseView {

    fun showError(error: SupportError)
}
