package ru.mercherry.ui.screens.login

import ru.mercherry.base.BaseView
import ru.mercherry.data.model.errors.AuthorizationError

interface LoginView : BaseView {
    fun showError(error: AuthorizationError)
}
