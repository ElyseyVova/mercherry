package ru.mercherry.ui.screens.info

import android.text.Html
import com.arellomobile.mvp.InjectViewState
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.model.Job
import ru.mercherry.data.repository.MercherryRepository
import javax.inject.Inject

@InjectViewState
class InfoPresenter @Inject constructor(
    private val repository: MercherryRepository
) : BasePresenter<InfoView>() {

    fun setInfoType(type: String) {
        when(type) {
            "offer" -> viewState.setInfoTitle("Пользовательское соглашение")
            "personal" -> viewState.setInfoTitle("Политика конфиденциальности")
        }

        fetchInfo(type)
    }


    // ===============================================================================================================================================
    // API
    // ===============================================================================================================================================

    private fun fetchInfo(type: String) {
        repository.info(
            type = type,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessInfo,
            onError = viewState::showError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun proceedSuccessInfo(info: String) {
        viewState.setInfoBody(info)
    }
}