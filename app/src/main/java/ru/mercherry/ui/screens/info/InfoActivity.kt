package ru.mercherry.ui.screens.info

import android.content.Context
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.activity_info.*
import org.jetbrains.anko.intentFor
import ru.mercherry.R
import ru.mercherry.base.BaseActivity
import ru.mercherry.utils.extensions.instance

class InfoActivity : BaseActivity(), InfoView {

    companion object {
        fun intent(ctx: Context, type: String) = ctx.intentFor<InfoActivity>("type" to type)
    }


    @InjectPresenter
    lateinit var presenter: InfoPresenter


    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        setupToolbar()
        setupUI()
        presenter.setInfoType(intent.getStringExtra("type"))
    }

    // ===============================================================================================================================================
    // View
    // ===============================================================================================================================================

    override fun setInfoTitle(title: String) {
        toolbar.title = title
    }

    override fun setInfoBody(body: String) {
        wvInfo.settings.javaScriptEnabled = true
        wvInfo.loadData(body, "text/html; charset=UTF-8", null);
    }


    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = instance<InfoPresenter>()

}
