package ru.mercherry.ui.screens.jobs

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_jobs.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.sdk27.coroutines.onTouch
import ru.mercherry.base.BaseFragment
import ru.mercherry.data.model.Job
import ru.mercherry.ui.screens.jobs.adapter.JobsAdapter
import ru.mercherry.utils.extensions.instance
import ru.mercherry.utils.extensions.selectedPosition
import ru.mercherry.utils.extensions.slideInTop
import ru.mercherry.utils.extensions.text

class JobsFragment : BaseFragment(), JobsView {


    @InjectPresenter
    lateinit var presenter: JobsPresenter

    private val adapter: JobsAdapter by lazy {
        JobsAdapter(presenter::handleJobClick)
    }

    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override val layoutResId = ru.mercherry.R.layout.fragment_jobs

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    override fun setupUI() {
        super.setupUI()
        activity?.title = "Список вакансий"
        rvJobs.onLoadMore(presenter::handleLoadMore)
        rvJobs.adapter = adapter
        rvJobs.onTouch { v, event ->  if (viewFilter.isShown) viewFilter.slideInTop() }
        btnFilter.onClick { viewFilter.slideInTop() }
        btnSave.onClick {
            viewFilter.slideInTop()
            presenter.handleFilterSaveClick(etCity.text, etSalary.text, groupEmployment.selectedPosition) }
    }


    // ===============================================================================================================================================
    // View
    // ===============================================================================================================================================

    override fun setJobs(jobs: List<Job>) {
        rvJobs.clearIndexes()
        adapter.setItems(jobs)
    }

    override fun addJobs(jobs: List<Job>) {
        adapter.addItems(jobs)
    }


    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = instance<JobsPresenter>()
}
