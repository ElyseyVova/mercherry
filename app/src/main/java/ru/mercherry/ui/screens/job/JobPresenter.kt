package ru.mercherry.ui.screens.job

import android.app.Activity.RESULT_OK
import android.content.Intent
import com.arellomobile.mvp.InjectViewState
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.model.Job
import ru.mercherry.data.repository.MercherryRepository
import ru.mercherry.utils.Screen
import javax.inject.Inject

@InjectViewState
class JobPresenter @Inject constructor(
    private var job: Job,
    private val repository: MercherryRepository
) : BasePresenter<JobView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        fetchJob()
    }

    fun handleResponseJobClick() {
        if (job.hasQuestions) {
            viewState.addScreenForResult(Screen.JOB_RESPONSE, 100, job.questions)
        } else {
            responseJob(mapOf())
        }
    }

    fun handleResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 100 && resultCode == RESULT_OK) {
            responseJob(data?.getSerializableExtra("data") as Map<String, Int>)
        }
    }


    // ===============================================================================================================================================
    // API
    // ===============================================================================================================================================

    private fun fetchJob() {
        repository.job(
            jobId = job.id,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessJob,
            onError = viewState::showError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }

    private fun responseJob(answers: Map<String, Int>) {
        repository.responseJob(
            jobId = job.id,
            answers = answers,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessResponseJob,
            onError = viewState::showError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun proceedSuccessJob(job: Job) {
        this.job = job
        viewState.setJobInfo(job)
    }

    private fun proceedSuccessResponseJob() {
        fetchJob()
        viewState.showAlertMessage("Вы откликнулись на вакансию. Статус по вашему отклику смотрите в разделе вакансии в личном кабинете")
    }
}