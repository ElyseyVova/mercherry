package ru.mercherry.ui.screens.my_jobs

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_jobs.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.sdk27.coroutines.onTouch
import ru.mercherry.base.BaseFragment
import ru.mercherry.data.model.Job
import ru.mercherry.ui.screens.jobs.JobsPresenter
import ru.mercherry.ui.screens.jobs.adapter.JobsAdapter
import ru.mercherry.utils.extensions.instance
import ru.mercherry.utils.extensions.selectedPosition
import ru.mercherry.utils.extensions.slideInTop
import ru.mercherry.utils.extensions.text

class MyJobsFragment : BaseFragment(), MyJobsView {

    @InjectPresenter
    lateinit var presenter: MyJobsPresenter

    private val adapter: JobsAdapter by lazy {
        JobsAdapter(presenter::handleJobClick)
    }

    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override val layoutResId = ru.mercherry.R.layout.fragment_my_jobs

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.title = "Мои вакансии"
        setupUI()
    }


    override fun setupUI() {
        super.setupUI()
        rvJobs.onLoadMore(presenter::handleLoadMore)
        rvJobs.adapter = adapter
    }


    // ===============================================================================================================================================
    // View
    // ===============================================================================================================================================

    override fun setJobs(jobs: List<Job>) {
        rvJobs.clearIndexes()
        adapter.setItems(jobs)
    }

    override fun addJobs(jobs: List<Job>) {
        adapter.addItems(jobs)
    }


    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = instance<MyJobsPresenter>()
}
