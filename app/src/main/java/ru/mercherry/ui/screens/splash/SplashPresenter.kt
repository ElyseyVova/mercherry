package ru.mercherry.ui.screens.splash

import com.arellomobile.mvp.InjectViewState
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.repository.MercherryRepository
import ru.mercherry.utils.Screen
import ru.mercherry.utils.extensions.timer
import javax.inject.Inject

@InjectViewState
class SplashPresenter @Inject constructor(
    private val repository: MercherryRepository
) : BasePresenter<SplashView>() {

    init {
        timer(1000) {
            viewState.showScreen(Screen.MAIN)
        }
    }
}
