package ru.mercherry.ui.screens.main

import ru.mercherry.base.BaseView

interface MainView : BaseView {
    fun showAuthorizedItems(isAuthorized: Boolean)
}
