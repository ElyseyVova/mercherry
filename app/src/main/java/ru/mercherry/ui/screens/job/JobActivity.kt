package ru.mercherry.ui.screens.job

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.activity_job.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick
import ru.mercherry.R
import ru.mercherry.base.BaseActivity
import ru.mercherry.data.model.Job
import ru.mercherry.utils.extensions.hide
import ru.mercherry.utils.extensions.instance
import ru.mercherry.utils.extensions.show

class JobActivity : BaseActivity(), JobView {

    companion object {
        fun intent(ctx: Context, job: Job) = ctx.intentFor<JobActivity>("job" to job)
    }


    @InjectPresenter
    lateinit var presenter: JobPresenter


    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job)
        setupToolbar()
        setupUI()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.handleResult(requestCode, resultCode, data)
    }

    override fun setupUI() {
        super.setupUI()
        btnResponseJob.onClick { presenter.handleResponseJobClick() }
    }


    // ===============================================================================================================================================
    // View
    // ===============================================================================================================================================

    override fun setJobInfo(job: Job) {
        toolbar.title = job.label
        tvLabel.text = job.label
        tvTotalSalary.text = "${if (job.totalSalary == 0) job.salary else job.totalSalary}\u20BD"
        tvLocation.text = job.fullLocation
        tvProject.text = job.project
        tvEmployment.text = job.employment
        tvType.text = job.type
        tvTypeProduct.text = job.productType
        tvEmployment.text = job.employment
        tvSalary.text = "${job.salary}\u20BD"
        tvPointCount.text = job.pointCount
        tvExperience.text = job.experience
        tvDurationInternship.text = job.durationInternship
        tvSchedule.text = job.schedule
        tvTotalSalary2.text = "${job.totalSalary}\u20BD"
        tvPrize.text = "${job.prize}\u20BD"
        tvMbook.text = "${if (job.mediceBook) "Да" else "Нет"}"
        tvMobile.text = "${if (job.mobile) "Да" else "Нет"}"
        tvAuto.text = "${if (job.auto) "Да" else "Нет"}"
        tvAudio.text = "${if (job.audioRecord) "Да" else "Нет"}"
        tvDescription.text = Html.fromHtml(job.description)

        if (job.response != null) {
            dividerStatusView.show()
            statusView.setResponse(job.response)
            btnResponseJob.hide()
        } else {
            btnResponseJob.show()
        }
    }


    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = JobPresenter(intent.extras.getSerializable("job") as Job, instance())
}
