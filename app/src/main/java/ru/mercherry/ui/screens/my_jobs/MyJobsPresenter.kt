package ru.mercherry.ui.screens.my_jobs

import android.text.Editable
import com.arellomobile.mvp.InjectViewState
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.model.Job
import ru.mercherry.data.model.response.JobsResponse
import ru.mercherry.data.repository.MercherryRepository
import ru.mercherry.ui.screens.jobs.JobsView
import ru.mercherry.utils.Screen
import javax.inject.Inject

@InjectViewState
class MyJobsPresenter @Inject constructor(
    private val repository: MercherryRepository
) : BasePresenter<MyJobsView>() {

    private var page = 1

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        fetchJobs()
    }

    fun handleJobClick(job: Job) {
        viewState.addScreen(Screen.JOB, job)
    }

    fun handleLoadMore() {
        fetchOldJobs()
    }


    // ===============================================================================================================================================
    // API
    // ===============================================================================================================================================

    private fun fetchJobs() {
        page = 1
        repository.myJobs(
            page = page,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessJobs,
            onEmpty = viewState::showEmpty,
            onError = viewState::showError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }

    private fun fetchOldJobs() {
        repository.myJobs(
            page = page,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessOldJobs,
            onError = viewState::showError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun proceedSuccessJobs(response: JobsResponse) {
        page++
        viewState.showContent()
        viewState.setJobs(response.result.reversed())
    }

    private fun proceedSuccessOldJobs(response: JobsResponse) {
        page++
        viewState.addJobs(response.result.reversed())
    }
}
