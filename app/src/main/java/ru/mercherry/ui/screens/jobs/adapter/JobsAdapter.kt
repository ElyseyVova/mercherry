package ru.mercherry.ui.screens.jobs.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.mercherry.data.model.Job

class JobsAdapter(var listener: (Job) -> Unit) : RecyclerView.Adapter<JobVH>() {

    private var data = mutableListOf<Job>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = JobVH(parent, listener)

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: JobVH, position: Int) {
        holder.bind(data[position])
    }

    fun setItems(jobs: List<Job>) {
        data = jobs.toMutableList()
        notifyDataSetChanged()
    }

    fun addItems(jobs: List<Job>) {
        val index = data.size
        data.addAll(jobs)
        notifyItemRangeInserted(index, data.size - 1)
    }
}
