package ru.mercherry.ui.screens.settings

import android.content.Context
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_settings.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick
import ru.mercherry.R
import ru.mercherry.base.BaseActivity
import ru.mercherry.data.model.User
import ru.mercherry.data.model.errors.SettingsError
import ru.mercherry.data.model.errors.SettingsError.*
import ru.mercherry.data.model.errors.SupportError
import ru.mercherry.utils.extensions.*
import showSelectorAddPhoto

class SettingsActivity : BaseActivity(), SettingsView {

    companion object {
        fun intent(ctx: Context) = ctx.intentFor<SettingsActivity>()
    }


    @InjectPresenter
    lateinit var presenter: SettingsPresenter

    // ===============================================================================================================================================
    // Android
    // ===============================================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setupToolbar()
        setupUI()
    }

    override fun setupUI() {
        super.setupUI()
        etPhone.addMaskPhone()
        btnSaveMainInfo.onClick {
            inputNickname.isErrorEnabled = false
            inputFirstName.isErrorEnabled = false
            inputLastName.isErrorEnabled = false
            inputEmail.isErrorEnabled = false
            inputPhone.isErrorEnabled = false
            presenter.handleSaveMainClick(etNickname.text(), etFirstName.text(), etLastName.text(), etEmail.text(), etPhone.text()) }

        btnSavePassword.onClick {
            inputNewPassword.isErrorEnabled = false
            inputConfirmPassword.isErrorEnabled = false
            presenter.handleSaveNewPassword(etNewPassword.text(), etConfirmPassword.text()) }

        btnPhoto.onClick { presenter.handleChangeAvatarClick() }
    }

    // ===============================================================================================================================================
    // View
    // ===============================================================================================================================================

    override fun setUserInfo(user: User) {
        ivPhoto.loadImageAvatar(user.avatar)

        etNickname.setText(user.username)
        etFirstName.setText(user.firstName)
        etLastName.setText(user.lastName)
        etEmail.setText(user.email)
        etPhone.setText(user.phone)
    }

    override fun setAvatar(url: String) {
        ivPhoto.loadImageAvatar(url)
    }

    override fun showPhotoPicker() {
        takeImage(1, presenter::handlePhotoSelected)
    }

    override fun showError(error: SettingsError) {
        when (error) {
            NICKNAME -> showFieldError(inputNickname, error.text)
            EMAIL -> showFieldError(inputEmail, error.text)
            PASSWORD -> showFieldError(inputNewPassword, error.text)
            PASSWORD_REPEAT -> showFieldError(inputConfirmPassword, error.text)
        }
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun showFieldError(input: TextInputLayout, text: String) {
        input.isErrorEnabled = true
        input.error = text
    }


    // ===============================================================================================================================================
    // Moxy
    // ===============================================================================================================================================

    @ProvidePresenter
    fun providePresenter() = instance<SettingsPresenter>()
}
