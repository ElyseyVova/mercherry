package ru.mercherry.ui.screens.info

import ru.mercherry.base.BaseView
import ru.mercherry.data.model.Job
import ru.mercherry.data.model.errors.AuthorizationError

interface InfoView : BaseView {
    fun setInfoTitle(title: String)
    fun setInfoBody(body: String)
}
