package ru.mercherry.ui.screens.settings

import com.arellomobile.mvp.InjectViewState
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.model.User
import ru.mercherry.data.model.errors.SettingsError
import ru.mercherry.data.repository.MercherryRepository
import javax.inject.Inject

@InjectViewState
class SettingsPresenter @Inject constructor(
    private val repository: MercherryRepository
) : BasePresenter<SettingsView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        fetchProfile()
    }

    fun handleChangeAvatarClick() {
        viewState.showPhotoPicker()
    }

    fun handlePhotoSelected(pathFile: String) {
        saveAvatar(pathFile)
    }

    fun handleSaveMainClick(username: String, firstName: String, lastName: String, email: String, phone: String) {
        when {
            else -> saveMain(username, firstName, lastName, email, phone)
        }
    }

    fun handleSaveNewPassword(newPassword: String, confirmPassword: String) {
        when {
            else -> savePassword(newPassword, confirmPassword)
        }
    }


    // ===============================================================================================================================================
    // API
    // ===============================================================================================================================================

    private fun fetchProfile() {
        repository.profile(
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessProfile,
            onError = viewState::showError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }

    private fun saveMain(username: String, firstName: String, lastName: String, email: String, phone: String) {
        repository.updateProfile(
            username = username,
            firstName = firstName,
            lastName = lastName,
            email = email,
            phone = phone,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessUpdateProfile,
            onError = ::proceedError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }

    private fun savePassword(newPassword: String, confirmPassword: String) {
        repository.updatePassword(
            newPassword = newPassword,
            confirmPassword = confirmPassword,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessUpdatePassword,
            onError = ::proceedError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }

    private fun saveAvatar(pathFile: String) {
        repository.setAvatar(
            pathFile = pathFile,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessSaveAvatar,
            onError = {proceedError(SettingsError.FILE_SMALL)},
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun proceedSuccessProfile(user: User) {
        viewState.setUserInfo(user)
    }

    private fun proceedSuccessUpdateProfile() {
        viewState.showMessage("Данные профиля успешно измненены")
    }

    private fun proceedSuccessUpdatePassword() {
        viewState.showMessage("Пароль успешно изменен")
    }

    private fun proceedSuccessSaveAvatar(url: String) {
        viewState.showMessage("Аватар был изменен")
        viewState.setAvatar(url)
    }

    private fun proceedError(error: SettingsError) {
        when (error) {
            SettingsError.DEFAULT -> viewState.showMessage(error.text)
            SettingsError.FILE_SMALL -> viewState.showMessage(error.text)
            else -> viewState.showError(error)
        }
    }
}
