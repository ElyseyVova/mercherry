package ru.mercherry.ui.screens.jobs

import android.text.Editable
import com.arellomobile.mvp.InjectViewState
import ru.mercherry.base.BasePresenter
import ru.mercherry.data.model.Job
import ru.mercherry.data.model.response.JobsResponse
import ru.mercherry.data.repository.MercherryRepository
import ru.mercherry.utils.Screen
import javax.inject.Inject

@InjectViewState
class JobsPresenter @Inject constructor(
    private val repository: MercherryRepository
) : BasePresenter<JobsView>() {

    private var page = 1
    private var city: String? = null
    private var salary: Int? = null
    private var employment: Int? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        fetchJobs()
    }

    fun handleFilterSaveClick(city: Editable?, salary: Editable?, employment: Int?) {
        this.city = city?.toString()
        this.salary = if (salary?.isEmpty() == true) null else salary?.toString()?.toInt()
        this.employment = if (employment == -1) null else employment

        fetchJobs()
    }

    fun handleJobClick(job: Job) {
        viewState.addScreen(Screen.JOB, job)
    }

    fun handleLoadMore() {
        fetchOldJobs()
    }


    // ===============================================================================================================================================
    // API
    // ===============================================================================================================================================

    private fun fetchJobs() {
        page = 1
        repository.jobs(
            page = page,
            city = city,
            salary = salary,
            employment = employment,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessJobs,
            onEmpty = viewState::showEmpty,
            onError = viewState::showError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }

    private fun fetchOldJobs() {
        repository.jobs(
            page = page,
            city = city,
            salary = salary,
            employment = employment,
            onStart = viewState::showLoading,
            onSuccess = ::proceedSuccessOldJobs,
            onError = viewState::showError,
            onNetworkError = viewState::showNetworkErrorMessage,
            onFinally = viewState::hideLoading
        )
    }


    // ===============================================================================================================================================
    // Private
    // ===============================================================================================================================================

    private fun proceedSuccessJobs(response: JobsResponse) {
        page++
        viewState.showContent()
        viewState.setJobs(response.result)
    }

    private fun proceedSuccessOldJobs(response: JobsResponse) {
        page++
        viewState.addJobs(response.result)
    }
}
