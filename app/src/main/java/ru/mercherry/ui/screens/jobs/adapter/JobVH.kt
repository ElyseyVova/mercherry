package ru.mercherry.ui.screens.jobs.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_job.view.*
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.sdk27.coroutines.onClick
import ru.mercherry.R
import ru.mercherry.data.model.Job

class JobVH(parent: ViewGroup, val listener: (Job) -> Unit) : RecyclerView.ViewHolder(
    parent.context.layoutInflater.inflate(R.layout.item_job, parent, false)
) {

    fun bind(job: Job) = with(itemView) {
        tvLabel.text = job.label
        tvTotalSalary.text = "${job.totalSalary}\u20BD"
        tvLocation.text = job.city
        tvCompany.text = job.project
        tvEmployment.text = job.employment
        onClick { listener.invoke(job) }
    }
}
