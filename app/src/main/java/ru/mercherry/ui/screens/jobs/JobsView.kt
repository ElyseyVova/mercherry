package ru.mercherry.ui.screens.jobs

import ru.mercherry.base.BaseView
import ru.mercherry.data.model.Job
import ru.mercherry.data.model.errors.AuthorizationError

interface JobsView : BaseView {
    fun setJobs(jobs: List<Job>)
    fun addJobs(jobs: List<Job>)
}
