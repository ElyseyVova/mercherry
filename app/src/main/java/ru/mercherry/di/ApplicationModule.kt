package ru.mercherry.di

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.content.res.Resources
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import okhttp3.OkHttpClient
import okhttp3.internal.platform.Platform
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.mercherry.BuildConfig
import ru.mercherry.data.preferences.Preferences
import ru.mercherry.data.api.MercherryAPI
import ru.mercherry.data.repository.MercherryRepository
import ru.mercherry.data.repository.MercherryRepositoryImpl
import toothpick.config.Module
import java.util.concurrent.TimeUnit

class ApplicationModule(
    context: Context,
    domain: String
) : Module() {

    companion object {
        private val CALL_ADAPTER_FACTORY = RxJava2CallAdapterFactory.create()

        private val GSON = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

        private val CONVERTER_FACTORY = GsonConverterFactory.create(GSON)
    }

    private val prefs = Preferences(context)
    private val res = context.resources

    init {
        val okHttpInterceptorLogging = LoggingInterceptor.Builder()
            .loggable(BuildConfig.DEBUG)
            .setLevel(Level.BODY)
            .log(Platform.INFO)
            .tag("REST")
            .build()

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(okHttpInterceptorLogging)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()

        val api = Retrofit.Builder()
            .addCallAdapterFactory(CALL_ADAPTER_FACTORY)
            .addConverterFactory(CONVERTER_FACTORY)
            .baseUrl(domain)
            .client(okHttpClient)
            .build().create(MercherryAPI::class.java)

        bind(Preferences::class.java).toInstance(Preferences(context))

        bind(MercherryAPI::class.java).toInstance(api)

        bind(Context::class.java).toInstance(context)
        bind(Gson::class.java).toInstance(GSON)
        bind(Resources::class.java).toInstance(context.resources)
        bind(SharedPreferences::class.java).toInstance(context.getSharedPreferences(Preferences.Constants.PREFERENCES_NAME, MODE_PRIVATE))

        bind(MercherryRepository::class.java).toInstance(MercherryRepositoryImpl(api, prefs))
    }
}
