package ru.mercherry.di

import android.content.Context
import ru.mercherry.utils.Constants.DOMAIN
import ru.mercherry.utils.Constants.SCOPES_APP
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import ru.mercherry.di.ApplicationModule
import toothpick.Toothpick
import toothpick.configuration.Configuration

object DependencyInjection {

    fun configure(context: Context) {
        Toothpick.setConfiguration(getConfig())
        initScopes(context)
    }

    private fun getConfig() = Configuration.forDevelopment().preventMultipleRootScopes()

    private fun initScopes(context: Context) {
        Toothpick.openScope(SCOPES_APP).installModules(ApplicationModule(context, DOMAIN))
    }
}
