package ru.mercherry.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kaopiz.kprogresshud.KProgressHUD
import loader
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.act
import ru.mercherry.R
import ru.mercherry.base.moxy.MvpAndroidXFragment
import ru.mercherry.data.model.Job
import ru.mercherry.ui.screens.job.JobActivity
import ru.mercherry.ui.screens.jobs.JobsFragment
import ru.mercherry.utils.Screen
import ru.mercherry.utils.extensions.hide
import ru.mercherry.utils.extensions.inject
import ru.mercherry.utils.extensions.show

abstract class BaseFragment : MvpAndroidXFragment(), BaseView {

    protected abstract val layoutResId: Int

    private lateinit var progressDialog: KProgressHUD

    private var contentView: View? = null
    private var emptyView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        inject()
        return inflater.inflate(layoutResId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog = activity!!.loader()
        contentView = view.findViewById(R.id.contentView)
        emptyView = view.findViewById(R.id.emptyView)
    }

    override fun onPause() {
        super.onPause()
        progressDialog.dismiss()
    }

    override fun showPreloading() {
    }

    override fun hidePreloading() {
    }

    override fun showLoading() {
        activity?.runOnUiThread {
            progressDialog.show()
        }
    }

    override fun hideLoading() {
        activity?.runOnUiThread {
            progressDialog.dismiss()
        }
    }

    override fun showMessage(message: String) {
        activity?.runOnUiThread {
            context?.toast(message)
        }
    }

    override fun showErrorMessage(message: String) {
        activity?.runOnUiThread {
            context?.toast(message)
        }
    }

    override fun showAlertMessage(message: String) {
        context?.alert(
            message,
            "Внимание"
        ) { yesButton { } }?.show()
    }

    override fun hideKeyboard() {

    }

    override fun onBackClicked() {
        parentFragment?.childFragmentManager?.popBackStack()
    }

    override fun addScreenForResult(screen: Screen, requestCode: Int, vararg data: Any?) {
        startActivityForResult(
            when (screen) {
//                    Screen.EDIT -> EditActivity.intent(act)
                else -> null
            }
            , requestCode
        )
    }

    override fun addScreen(screen: Screen, vararg data: Any?) {
        startActivity(
            when (screen) {
                Screen.JOB -> JobActivity.intent(act, data[0] as Job)
                else -> null
            }
        )
    }

    override fun showScreen(screen: Screen) {
        act.finish()
        startActivity(
            when (screen) {
                else -> null
            }
        )
    }

    override fun showContent() {
        emptyView?.hide()
        contentView?.show()
    }

    override fun showError() {

    }

    override fun showEmpty() {
        emptyView?.show()
        contentView?.hide()
    }

    override fun showUnauthorized() {
    }

    override fun showNetworkError() {
    }

    override fun showNetworkErrorMessage() {
    }

    override fun closeScreen(message: String?) {

    }

    override fun closeScreenWithResult(resultCode: Int, message: String?) {

    }

    override fun closeScreenWithResult(resultCode: Int, vararg data: Any?) {

    }

    override fun restart() {

    }

    override fun setupUI() {

    }

    override fun setupToolbar() {

    }
}
