package ru.mercherry.base

import android.util.Log
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<View : BaseView>() : MvpPresenter<View>() {

    val destroyDisposable = CompositeDisposable()
    val disposables = CompositeDisposable()
    private val detachDisposable = CompositeDisposable()

    protected fun processError(throwable: Throwable) {
        throwable.apply { Log.d("LOGGER", "ERROR: ${throwable.localizedMessage}") }
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        addBusListener()
    }

    override fun attachView(view: View) {
        super.attachView(view)
        view.bind()
    }

    override fun detachView(view: View) {
        detachDisposable.clear()
        super.detachView(view)
    }

    override fun onDestroy() {
        destroyDisposable.clear()
        disposables.clear()
        super.onDestroy()
    }

    fun unsubscribeOnDestroy(d: Disposable) {
        destroyDisposable.add(d)
    }

    fun unsubscribeOnDetach(d: Disposable) {
        detachDisposable.add(d)
    }

    open fun handleBackClick() {
        viewState.onBackClicked()
    }

    open fun addBusListener() {

    }

    open fun proceedFailed() {
        viewState.showErrorMessage("Что-то пошло не так...")
    }
}
