package ru.mercherry.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kaopiz.kprogresshud.KProgressHUD
import loader
import org.jetbrains.anko.inputMethodManager
import org.jetbrains.anko.toast
import ru.mercherry.base.moxy.MvpAndroidXDialogFragment
import ru.mercherry.utils.Screen
import showDialogMessage

abstract class BaseDialogFragment : MvpAndroidXDialogFragment(), BaseView {

    protected abstract val layoutResId: Int

//    var errorView: ErrorView? = null
//    var emptyView: ErrorView? = null
//    var loadingView: View? = null

    private lateinit var progressDialog: KProgressHUD

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        progressDialog = context!!.loader()
        return inflater.inflate(layoutResId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        errorView = view.findOptional(R.userId.errorView)
//        emptyView = view.findOptional(R.userId.emptyView)
//        loadingView = view.findOptional(R.userId.loadingView)
    }

    override fun onPause() {
        super.onPause()
        progressDialog.dismiss()
    }

    override fun showPreloading() {
//        errorView?.hide()
//        loadingView?.show()
    }

    override fun hidePreloading() {
//        loadingView?.hide()
    }

    override fun showLoading() {
        progressDialog.show()
    }

    override fun hideLoading() {
        progressDialog.dismiss()
    }


    override fun showMessage(message: String) {
        activity?.toast(message)
    }

    override fun showAlertMessage(message: String) {
        activity?.showDialogMessage(message)
    }

    override fun hideKeyboard() {
        view?.let { this.hideKeyboard(it) }
    }

    override fun onBackClicked() {
        parentFragment?.childFragmentManager?.popBackStack()
    }

    override fun addScreen(screen: Screen, vararg data: Any?) {

    }

    override fun showScreen(screen: Screen) {

    }

    override fun showContent() {

    }

    override fun showError() {
//        errorView?.setMode(SERVER)
//        errorView?.show()
    }

    override fun showErrorMessage(message: String) {
        activity?.toast(message)
    }

    override fun showNetworkError() {
//        errorView?.setMode(NETWORK)
//        errorView?.show()
    }

    override fun showNetworkErrorMessage() {
    }

    override fun showEmpty() {
//        emptyView?.show()
    }

    override fun closeScreen(message: String?) {
        message?.apply { showMessage(message) }
        dismiss()
    }

    private fun hideKeyboard(view: View) {
        activity?.inputMethodManager?.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
