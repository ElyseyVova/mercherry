package ru.mercherry.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kaopiz.kprogresshud.KProgressHUD
import loader
import org.jetbrains.anko.inputMethodManager
import org.jetbrains.anko.support.v4.act
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import ru.mercherry.utils.extensions.restartApp
import ru.autohelp.utils.extensions.str
import ru.mercherry.R
import ru.mercherry.base.moxy.MvpBottomSheetDialogFragment
import ru.mercherry.utils.Screen

abstract class BaseBottomSheetDialogFragment : MvpBottomSheetDialogFragment(), BaseView {

    protected abstract val layoutResId: Int

    private lateinit var progressDialog: KProgressHUD

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        progressDialog = ctx.loader()
        return inflater.inflate(layoutResId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onPause() {
        super.onPause()
        progressDialog.dismiss()
    }

    override fun setupToolbar() {

    }

    override fun showPreloading() {
    }

    override fun hidePreloading() {
    }

    override fun showLoading() {
        progressDialog.show()
    }

    override fun hideLoading() {
        progressDialog.dismiss()
    }


    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showAlertMessage(message: String) {
        alert(
            message,
            ctx.str(R.string.label_alert_attention)
        ) { yesButton { } }.show()
    }

    override fun hideKeyboard() {
        view?.let { this.hideKeyboard(it) }
    }

    override fun onBackClicked() {
        parentFragment?.childFragmentManager?.popBackStack()
    }

    override fun addScreen(screen: Screen, vararg data: Any?) {
        startActivity(
            when (screen) {
                else -> null
            }
        )
    }

    override fun showScreen(screen: Screen) {
        act.finish()
        startActivity(
            when (screen) {
                else -> null
            }
        )
    }

    override fun addScreenForResult(screen: Screen, requestCode: Int, vararg data: Any?) {

    }

    override fun closeScreenWithResult(resultCode: Int, message: String?) {

    }

    override fun showContent() {

    }

    override fun showError() {
    }

    override fun showUnauthorized() {

    }

    override fun showErrorMessage(message: String) {
        ctx.toast(message)
    }

    override fun showNetworkError() {
    }

    override fun showNetworkErrorMessage() {
        showMessage(ctx.str(R.string.error_load))
    }

    override fun showEmpty() {
//        emptyView?.show()
    }

    override fun closeScreen(message: String?) {
        message?.apply { showMessage(message) }
        dismiss()
    }

    override fun restart() {
        act.restartApp()
    }

    private fun hideKeyboard(view: View) {
        ctx.inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
