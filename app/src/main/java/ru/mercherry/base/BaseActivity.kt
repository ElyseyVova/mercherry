package ru.mercherry.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.kaopiz.kprogresshud.KProgressHUD
import loader
import org.jetbrains.anko.find
import org.jetbrains.anko.findOptional
import org.jetbrains.anko.toast
import ru.mercherry.R
import ru.mercherry.base.moxy.MvpAndroidXActivity
import ru.mercherry.ui.screens.info.InfoActivity
import ru.mercherry.ui.screens.job.JobResponseActivity
import ru.mercherry.ui.screens.jobs.JobsFragment
import ru.mercherry.ui.screens.login.LoginActivity
import ru.mercherry.ui.screens.main.MainActivity
import ru.mercherry.ui.screens.my_jobs.MyJobsFragment
import ru.mercherry.ui.screens.registration.RegistrationActivity
import ru.mercherry.ui.screens.settings.SettingsActivity
import ru.mercherry.ui.screens.support.SupportActivity
import ru.mercherry.utils.Screen
import ru.mercherry.utils.extensions.enableHideKeyboard
import ru.mercherry.utils.extensions.hide
import ru.mercherry.utils.extensions.restartApp
import ru.mercherry.utils.extensions.show
import showDialogMessage
import java.io.Serializable

abstract class BaseActivity : MvpAndroidXActivity(), BaseView {

    private lateinit var loader: KProgressHUD

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loader = loader()
    }

    override fun setupToolbar() {
        (findViewById<Toolbar>(R.id.toolbar)).setNavigationOnClickListener { finish() }
    }

    override fun setupUI() {
        findOptional<View>(R.id.root)?.enableHideKeyboard()
    }

    override fun onPause() {
        super.onPause()
        loader.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        loader.dismiss()
    }

    override fun showPreloading() {

    }

    override fun hidePreloading() {
    }

    override fun showLoading() {
        loader.show()
    }

    override fun hideLoading() {
        loader.dismiss()
    }

    override fun showContent() {
        find<View>(R.id.contentView).show()
//        find<View>(R.id.emptyView).hide()
    }

    override fun showError() {

    }

    override fun showEmpty() {
        find<View>(R.id.contentView).hide()
//        find<View>(R.id.emptyView).show()
    }

    override fun showUnauthorized() {
    }

    override fun showNetworkError() {

    }

    override fun showNetworkErrorMessage() {
    }

    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showErrorMessage(message: String) {
        toast(message)
    }

    override fun showAlertMessage(message: String) {
        showDialogMessage(message)
    }

    override fun hideKeyboard() {

    }

    override fun onBackClicked() {
        finish()
    }

    override fun addScreenForResult(screen: Screen, requestCode: Int, vararg data: Any?) {
        loader.dismiss()
        startActivityForResult(
            when (screen) {
                Screen.AUTHORIZATION -> LoginActivity.intent(this)
                Screen.REGISTRATION -> RegistrationActivity.intent(this)
                Screen.SETTINGS -> SettingsActivity.intent(this)
                Screen.JOB_RESPONSE -> JobResponseActivity.intent(this, data[0] as Map<String, String>)
                else -> null
            }, requestCode
        )
    }

    override fun addScreen(screen: Screen, vararg data: Any?) {
        loader.dismiss()
        when (screen) {
            Screen.JOBS -> supportFragmentManager.beginTransaction().replace(R.id.container, JobsFragment()).commit()
            Screen.MY_JOBS -> supportFragmentManager.beginTransaction().replace(R.id.container, MyJobsFragment()).commit()
            Screen.SETTINGS -> startActivity(SettingsActivity.intent(this))
            Screen.SUPPORT -> startActivity(SupportActivity.intent(this))
            Screen.INFO -> startActivity(InfoActivity.intent(this, data[0] as String))
        }

    }

    override fun showScreen(screen: Screen) {
        loader.dismiss()
        finish()
        startActivity(
            when (screen) {
                Screen.MAIN -> MainActivity.intent(this)
                Screen.REGISTRATION -> RegistrationActivity.intent(this)
                else -> null
            }
        )
    }

    override fun closeScreen(message: String?) {
        message?.let { showMessage(it) }
        finish()
    }

    override fun closeScreenWithResult(resultCode: Int, message: String?) {
        message?.let { showMessage(it) }
        setResult(resultCode)
        finish()
    }

    override fun closeScreenWithResult(resultCode: Int, vararg data: Any?) {
        if (data != null && data[0] != null) {
            setResult(resultCode, Intent().apply { putExtra("data", data[0] as Serializable) })
        } else {
            setResult(resultCode)
        }

        finish()
    }

    override fun restart() {
        restartApp()
    }
}
