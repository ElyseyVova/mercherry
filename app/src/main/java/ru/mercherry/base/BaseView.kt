package ru.mercherry.base

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.mercherry.utils.Screen

interface BaseView : MvpView {

    fun bind() {}

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showPreloading()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoading()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun hidePreloading()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun hideLoading()

    @StateStrategyType(SkipStrategy::class)
    fun showAlertMessage(message: String)

    fun showMessage(message: String)

    fun showErrorMessage(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun hideKeyboard()

    fun onBackClicked()

    fun addScreenForResult(screen: Screen, requestCode: Int, vararg data: Any?)

    fun addScreen(screen: Screen, vararg data: Any?)

    fun showScreen(screen: Screen)

    fun closeScreen(message: String? = null)

    fun closeScreenWithResult(resultCode: Int, message: String? = null)

    fun closeScreenWithResult(resultCode: Int, vararg data: Any?)

    fun showError()

    fun showNetworkError()

    fun showNetworkErrorMessage()

    fun showEmpty()

    fun showContent()
    fun showUnauthorized()

    fun restart()
    fun setupUI()
    fun setupToolbar()
}
